<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class feedbacks{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createFeedback($date, $rating, $review, $customer, $provider){
			if($this->isSubscribed($customer, $provider)){
				$stmt=$this->con->prepare("insert into tbl_feedback (feedback_date, feedback_rating, 
				feedback_review, prof_id, prov_id) values (?, ?, ?, ?, ?)");
				$stmt->bind_param("sssss", $date, $rating, $review, $customer, $provider);
					if($stmt->execute())
						return 1;
					else
						return 2;
			}
			else{
				return 0;
			}		
		}
		
		
		public function deleteFeedback($id){
			$stmt=$this->con->prepare("delete from tbl_feedback where feedback_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 2;
		}
		public function editFeedback($rev, $id){
			$stmt=$this->con->prepare("update tbl_feedback set feedback_review=? where feedback_id=?");
			$stmt->bind_param("ss", $rev,$id);
				if($stmt->execute())
					return 1;
				else
					return 2;
		}
		public function updateFeedback($rev, $rat, $id){
			$stmt=$this->con->prepare("update tbl_feedback set feedback_review=?, 
			feedback_rating=? where feedback_id=?");
			$stmt->bind_param("sss", $rev, $rat, $id);
				if($stmt->execute())
					return 1;
				else
					return 2;
		}
		
		public function isSubscribed($customer, $provider){
			$stmt=$this->con->prepare("select * from request where 
			prof_id=? and prov_id=?");
			$stmt->bind_param("ss", $customer, $provider);
			$stmt->execute();
			$stmt->store_result();
			return $stmt->num_rows > 0;
		}
		
		
		public function getFeedbackByProvider($id){
			$stmt=$this->con->prepare("select feedback_id, feedback_date, feedback_rating, 
			feedback_review, prof_last, prof_first, prov_id, prof_user, prof_pic
			from tbl_feedback
			inner join prof on
			tbl_feedback.prof_id=prof.prof_id
			where prov_id=? order by feedback_id desc");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $date, $rating, $review, $customerL, $customerF, $provider, $username, $u);
			$feedback=array();
				while($stmt->fetch()){
					$name = $customerF." ".$customerL;
					$temp=array();
					$temp['id']=$id;
					$temp['date']=$date;
					$temp['rating']=$rating;
					$temp['review']=$review;
					$temp['customer']=$name;
					$temp['provider']=$provider;
					$temp['username']=$username;
					$temp['url']=$u;
					array_push($feedback, $temp);
				}
				return $feedback;
		}
		
	}	
?>