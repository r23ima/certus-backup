<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class sched{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createSched($exam_id, $start_date, $end_date, $valid_to, $valid_from, $stat){
			$stmt = $this->con->prepare("insert into sched (exam_id, start_date, end_date, valid_to, valid_from, status) values (?,?,?,?,?,?)");
			$stmt->bind_param("ssssss", $exam_id, $start_date, $end_date, $valid_to, $valid_from,$stat);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function editSched($start_date, $end_date, $valid_to, $valid_from, $sched_id){
			$stmt = $this->con->prepare("update sched set start_date=?, end_date=?, valid_to=?, valid_from=? where sched_id=?");
			$stmt->bind_param("sssss",$start_date, $end_date, $valid_to, $valid_from, $sched_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteSched($id){
			$stmt = $this->con->prepare("delete from sched where sched_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewAllSched($id, $s){
			if($s==''){
				$q = "select * from sched where exam_id=?";
				$stmt = $this->con->prepare($q);
				$stmt->bind_param("s", $id);
			}
			else{
				$q = "select * from sched where exam_id=? and status=?";
				$stmt = $this->con->prepare($q);
				$stmt->bind_param("ss", $id, $s);
			}

			$stmt->execute();
			$stmt->bind_result($exam_id, $sched_id, $start_date, $end_date, $valid_to, $valid_from, $stat);
			$sched=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['sched_id'] = $exam_id;
					$temp['exam_id'] = $exam_id;
					$temp['start'] = $start_date;
					$temp['end'] = $end_date;
					$temp['to'] = $valid_to;
					$temp['from'] = $valid_from;
					$temp['stat'] = $stat;
					array_push($sched, $temp);
				}
			return $sched;	
		}
	}
?>
