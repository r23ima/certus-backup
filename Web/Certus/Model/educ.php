<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class educ{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		public function createEduc($prof_id, $school, $degree, $educ_start, $educ_end, $educ_award, $educ_lvl){
			$stmt = $this->con->prepare("insert into educ (prof_id, school, degree, educ_start, educ_end, educ_award, educ_lvl) values (?,?,?,?,?,?,?)");
			$stmt->bind_param("sssssss", $prof_id, $school, $degree, $educ_start, $educ_end, $educ_award, $educ_lvl);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function editEduc($school, $educ_start, $educ_end, $educ_award, $educ_degree, $educ_id){
			$stmt = $this->con->prepare("update educ set school=?, educ_start=?, educ_end=?, educ_award=?, degree=? where educ_id=?");
			$stmt->bind_param("ssssss", $school, $educ_start, $educ_end, $educ_award, $educ_degree, $educ_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteEduc($id){
			$stmt = $this->con->prepare("delete from educ where educ_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewByLvl($id, $lv){
			$stmt=$this->con->prepare("select * from educ where prof_id=? and educ_lvl=?");
			$stmt->bind_param("ss", $id, $lv);
			$stmt->execute();
			$stmt->bind_result($educ_id, $prof_id, $school, $degree, $educ_start, $educ_end, $educ_award, $educ_lvl);
			$edu=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $educ_id;
					$temp['prof_id'] = $prof_id;
					$temp['school'] = $school;
					$temp['degree'] = $degree;
					$temp['start'] = $educ_start;
					$temp['end'] = $educ_end;
					$temp['award'] = $educ_award;
					$temp['lvl'] = $educ_lvl;
					array_push($edu, $temp);
				}
			return $edu;
		}

		public function viewAll($id){
			$stmt=$this->con->prepare("select * from educ where prof_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($educ_id, $prof_id, $school, $degree, $educ_start, $educ_end, $educ_award, $educ_lvl);
			$edu=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $educ_id;
					$temp['prof_id'] = $prof_id;
					$temp['school'] = $school;
					$temp['degree'] = $degree;
					$temp['start'] = $educ_start;
					$temp['end'] = $educ_end;
					$temp['award'] = $educ_award;
					$temp['lvl'] = $educ_lvl;
					array_push($edu, $temp);
				}
			return $edu;
		}
		
	}
?>