<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class payment{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createPayment($id, $date, $amount){
			$stmt = $this->con->prepare("insert into payment (request_id, date, amount) values (?,?,?)");
			$stmt->bind_param("sss", $id, $date, $amount);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}

		public function editPayment($amount, $id){
			$stmt = $this->con->prepare("update payment set amount=? where payment_id=?");
			$stmt->bind_param("ssss", $name, $type, $dis, $paytype_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deletePaytype($id){
			$stmt = $this->con->prepare("delete from pay_type where paytype_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewAllPayment($id){
			$stmt = $this->con->prepare("select * from payment where request_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($pay_id, $request_id, $date, $amount);
			$payment=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['pay_id'] 		= $pay_id;
					$temp['request_id']	 	= $request_id;
					$temp['date'] 			= $date;
					$temp['amount'] 		= $amount;
					array_push($payment, $temp);
				}
			return $payment;	
		}
		
	}
?>