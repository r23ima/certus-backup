<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class notif{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function receiveNotificationFromStudents($id){
			$stmt = $this->con->prepare(
				"select request_id, date, concat(prof_first, ' ', prof_last) 
				 as fullname, prof_pic, request.status from  request
				 inner join prof
				 on prof.prof_id = request.prof_id
				 where prov_id=? order by request_id desc");

			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $date, $fullname, $pic, $status);
			$notif=array();
				while($stmt->fetch()){
					if($status=="PENDING" || $status=="CANCELLED"){
						$temp=array();
						$temp['id'] = $id;
						$temp['date'] = $date;
						$temp['full'] = $fullname;
						$temp['pic'] = $pic;
						$temp['stat'] = $status;
						array_push($notif, $temp);
					}
				}
			return $notif;	
		}

		public function receiveNotificationFromProvider($id){
			$stmt = $this->con->prepare(
				"select request_id, date, prov_name, prov_pic, request.status from  request
				 inner join provider
				 on provider.prov_id = request.prov_id
				 where request.prof_id=? order by request_id desc");

			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $date, $fullname, $pic, $status);
			$notif=array();
				while($stmt->fetch()){
					if($status=="APPROVE" || $status=="DECLINE"){
						$temp=array();
						$temp['id'] = $id;
						$temp['date'] = $date;
						$temp['full'] = $fullname;
						$temp['pic'] = $pic;
						$temp['stat'] = $status;
						array_push($notif, $temp);
					}
				}
			return $notif;	
		}

	}
?>