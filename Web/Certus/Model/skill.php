<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class skill{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createSkill($prof_id, $descr, $special){
			$stmt = $this->con->prepare("insert into skill (prof_id, descr, special) values (?,?,?)");
			$stmt->bind_param("sss", $prof_id, $descr, $special);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function editSkill($special, $skill_id){
			$stmt = $this->con->prepare("update skill set special=? where skill_id=?");
			$stmt->bind_param("ss", $special, $skill_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteSkill($id){
			$stmt = $this->con->prepare("delete from skill where skill_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewAllSkill($id){
			$stmt = $this->con->prepare("select * from skill where prof_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $prof_id, $descr, $sp);
			$skill=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $id;
					$temp['prof_id'] = $prof_id;
					$temp['descr'] = $descr;
					$temp['sp'] = $sp;
					array_push($skill, $temp);
				}
			return $skill;	
		}
		
	}
?>