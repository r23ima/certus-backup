<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class prov{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createProv($name, $addr, $email, $user, $pass, $pic, $lat, $lon, $stat){
			$stmt = $this->con->prepare("insert into provider (prov_name, prov_addr, prov_email, prov_user, prov_pass, prov_pic, prov_lat, prov_lon, prov_stat) values (?,?,?,?,?,?,?,?,?)");
			$stmt->bind_param("sssssssss", $name, $addr, $email, $user, $pass, $pic, $lat, $lon, $stat);
				if($stmt->execute())
					return $last_id = $this->con->insert_id;
				else
					return 0;
		}
		public function editProv($name, $addr, $id){
			$stmt = $this->con->prepare("update provider set prov_name=?, prov_addr=? where prov_id=?");
			$stmt->bind_param("sss", $name, $addr, $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteProv($id, $stat){
			$stmt = $this->con->prepare("update provider set prov_stat=? where prov_id=?");
			$stmt->bind_param("ss", $stat, $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}

		public function getAllActive(){
			$char="Active";
			$stmt = $this->con->prepare("select * from provider where prov_stat=?");
			$stmt->bind_param("s", $char);
			$stmt->execute();
			$stmt->bind_result($id, $name, $addr, $email, $user, $pass, $pic, $lat, $lon, $stat);
			$provider = array();
			while($stmt->fetch()){
				$temp = array();
				$temp['id'] = $id;
				$temp['name'] = $name;
				$temp['addr'] = $addr;
				$temp['email'] = $email;
				$temp['pic'] = $pic;
				$temp['lat'] = $lat;
				$temp['lon'] = $lon;

				array_push($provider, $temp);
			}

			return $provider;
		}
		
	}
?>