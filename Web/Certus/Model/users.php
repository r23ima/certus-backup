<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class users{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function getProf($username, $password){
			$char="Deactivated";
			$p=md5($password);
			$stmt = $this->con->prepare(
			"select * from prof where prof_user=? and prof_pass=? and prof_stat!=?");
			$stmt->bind_param("sss",$username, $p, $char);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function getAdmin($username, $password){
			$p=md5($password);
			$stmt = $this->con->prepare(
			"select * from admin where admin_user=? and admin_pass=?");
			$stmt->bind_param("ss",$username, $p);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function getProvider($username, $password){
			$char="Deactivated";
			$p=md5($password);
			$stmt = $this->con->prepare(
			"select * from provider where prov_user=? and prov_pass=? and prov_stat!=?");
			$stmt->bind_param("sss",$username, $p, $char);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}

		public function getProf_details($username){
			$stmt = $this->con->prepare(
			"select * from prof where prof_user=?");
			$stmt->bind_param("s",$username);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function getAdmin_details($username){
			$stmt = $this->con->prepare(
			"select * from admin where admin_user=?");
			$stmt->bind_param("s",$username);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
		public function getProvider_details($id){
			$stmt = $this->con->prepare(
			"select * from provider where prov_id=?");
			$stmt->bind_param("s",$id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
	}
?>