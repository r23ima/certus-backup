<?php
	define('DB_NAME', 'certus');
	define('DB_USER','root');
	define('DB_PASSWORD','');
	define('DB_HOST','localhost');
	
	class DbConnect{
		private $con;
		
		function __construct(){
			
		}
		
		function connect(){
			$this->con = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);		
			if(mysqli_connect_errno()){
				echo "Failed to connect with database".mysqli_connect_error();
			}			
			return $this->con;
		}
	}
?>