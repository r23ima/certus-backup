<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class prof{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}

		public function createProf($last, $first, $birth, $gender, $addr, $email, $user, $pass, $stat, $pic, $lat, $lon){
			$stmt = $this->con->prepare("insert into prof (prof_last, prof_first, prof_birth, prof_gender, prof_addr, prof_email, prof_user, prof_pass, prof_stat, prof_pic, prof_lat, prof_lon) values (?,?,?,?,?,?,?,?,?,?,?,?)");
			$stmt->bind_param("ssssssssssss", $last, $first, $birth, $gender, $addr, $email, $user, $pass, $stat, $pic, $lat, $lon);
				if($stmt->execute())
					return $last_id = $this->con->insert_id;
				else
					return 0;
		}
		public function editProf($last, $first, $addr, $id){
			$stmt = $this->con->prepare("update prof set prof_last=?, prof_first=?, prof_addr=? where prof_id=?");
			$stmt->bind_param("ssss", $last, $first, $addr, $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteProf($id, $stat){
			$stmt = $this->con->prepare("update prof set prof_stat=? where prof_id=?");
			$stmt->bind_param("ss", $stat, $id);
			if($stmt->execute())
					return 1;
				else
					return 0;
		}

		public function getProf_details($username){
			$stmt = $this->con->prepare(
			"select * from prof where prof_user=?");
			$stmt->bind_param("s",$username);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}
		
	}
?>