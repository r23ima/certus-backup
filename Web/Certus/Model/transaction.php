<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class trans{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function viewAllTransaction($iden){
			$stmt = $this->con->prepare(
				"select request_id, date, concat(prof_first, ' ', prof_last) 
				 as fullname, prof_pic, request.status, total from  request
				 inner join prof
				 on prof.prof_id = request.prof_id
				 where prov_id=? order by request_id desc");

			$stmt->bind_param("s", $iden);
			$stmt->execute();
			$stmt->bind_result($id, $date, $fullname, $pic, $status, $t);
			$request=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $id;
					$temp['date'] = $date;
					$temp['full'] = $fullname;
					$temp['pic'] = $pic;
					$temp['stat'] = $status;
					$temp['total'] = $t;
					array_push($request, $temp);
				}
			return $request;	
		}
		
		public function viewYourSingleRequest($id){
			$stmt = $this->con->prepare(
				"select request_id, date, prov_name, prov_pic, prov_addr, prov_email, descr, request.status, pay_type.name, total, discount, message, prov_lat, prov_lon, prov_addr from request inner join provider on provider.prov_id = request.prov_id inner join pay_type on pay_type.paytype_id = request.paytype_id where request_id=? limit 1
				");

			$stmt->bind_param("s", $id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();	
		}

		public function viewCourses($id){
			$stmt = $this->con->prepare("
				select exam.exam_id, name, lvl, rate
				from exam inner join taker
				on exam.exam_id = taker.exam_id
				inner join request
				on taker.request_id = request.request_id
				where taker.request_id = ?
				");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $name, $lvl, $rate);
			$courses=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $id;
					$temp['name'] = $name;
					$temp['lvl'] = $lvl;
					$temp['rate'] = $rate;
					array_push($courses, $temp);
				}
			return $courses;	
		}

		//change to ongoing transaction/portfolio of the user
		public function viewOngoing($id){
			$stmt = $this->con->prepare("
				select exam.exam_id, name, lvl, rate, remarks, result
				from exam inner join taker
				on exam.exam_id = taker.exam_id
				inner join request
				on taker.request_id = request.request_id
				where taker.request_id = ?
				");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $name, $lvl, $rate, $remarks, $result);
			$courses=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $id;
					$temp['name'] = $name;
					$temp['lvl'] = $lvl;
					$temp['rate'] = $rate;
					$temp['remarks'] = $remarks;
					$temp['result'] = $result;
					array_push($courses, $temp);
				}
			return $courses;	
		}
	}
?>