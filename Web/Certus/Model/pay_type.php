<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class paytype{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createPaytype($name, $type, $dis, $id){
			$stmt = $this->con->prepare("insert into pay_type (name, type, discount, prov_id) values (?,?,?,?)");
			$stmt->bind_param("ssss", $name, $type, $dis, $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function editPaytype($name, $type, $dis, $paytype_id){
			$stmt = $this->con->prepare("update pay_type set name=?, type=?, $discount=? where paytype_id=?");
			$stmt->bind_param("ssss", $name, $type, $dis, $paytype_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deletePaytype($id){
			$stmt = $this->con->prepare("delete from pay_type where paytype_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewAllPaytype($id){
			$stmt = $this->con->prepare("select * from pay_type where prov_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $name, $type, $dis, $prov);
			$paytype=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['paytype_id'] = $id;
					$temp['name']	 	= $name;
					$temp['type'] 		= $type;
					$temp['dis'] 		= $dis;
					$temp['prov_id'] 	= $prov;
					array_push($paytype, $temp);
				}
			return $paytype;	
		}
		
	}
?>