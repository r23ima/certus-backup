<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class exam{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createExam($prov_id, $name, $descr, $lvl, $slots, $rate, $prerequisit, $duration, $req_doc){
			$stmt = $this->con->prepare("insert into exam (prov_id, name, descr, lvl, slots, rate, prerequisit, valid_to, valid_from, 
			duration, req_doc) values (?,?,?,?,?,?,?,?,?)");
			$stmt->bind_param("sssssssss", $prov_id, $name, $descr, $lvl, $slots, $rate, $prerequisit, $duration, $req_doc);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function editExam($exam_id, $name, $descr, $lvl, $slots, $rate, $prerequisit, $duration, $req_doc){
			$stmt = $this->con->prepare("update exam set name=?, descr=?, lvl=?, slots=?, rate=?, prerequisit=?, duration=?, req_doc=? where exam_id=?");
			$stmt->bind_param("sssssssss", $name, $descr, $lvl, $slots, $rate, $prerequisit, $duration, $req_doc, $exam_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteExam($id){
			$stmt = $this->con->prepare("delete from exam where exam_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewAllExam($id){
			$stmt = $this->con->prepare("select * from exam where prov_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($exam_id, $name, $descr, $lvl, $slots, $rate, $prerequisit, $duration, $req_doc, $prov_id, $stat, $to, $from);
			$exam=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['exam_id'] = $exam_id;
					$temp['prov_id'] = $prov_id;
					$temp['name'] = $name;
					$temp['descr'] = $descr;
					$temp['lvl'] = $lvl;
					$temp['slots'] = $slots;
					$temp['rate'] = $rate;
					$temp['pre'] = $prerequisit;
					$temp['duration'] = $duration;
					$temp['req_doc'] = $req_doc;
					$temp['stat'] = $stat;
					$temp['to'] = $to;
					$temp['from'] = $from;
					array_push($exam, $temp);
				}
			return $exam;	
		}
		public function viewAllStudentsByExam($id){
			$stmt = $this->con->prepare("select taker_id, prof.prof_id, concat (prof_first, ' ', prof_last) as fullname, prof_addr, prof_email, prof_gender, name, lvl, remarks, result, taker.status from prof inner join request on prof.prof_id = request.prof_id inner join taker on taker.request_id = request.request_id inner join exam on exam.exam_id = taker.exam_id where taker.exam_id = ?
			");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($taker_id, $prof_id, $fullname, $addr, $email, $gender, $cert_name, $lvl, $remarks, $result, $taker_stat);
			$student=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['taker_id'] = $taker_id;
					$temp['prof_id'] = $prof_id;
					$temp['fullname'] = $fullname;
					$temp['addr'] = $addr;
					$temp['email'] = $email;
					$temp['gender'] = $gender;
					$temp['cert_name'] = $cert_name;
					$temp['lvl'] = $lvl;
					$temp['remarks'] = $remarks;
					$temp['result'] = $result;
					$temp['taker_stat'] = $taker_stat;
					array_push($student, $temp);
				}
			return $student;	
		}
		public function updateGrades($result, $remarks, $id){
			$stmt = $this->con->prepare("update taker set result=?, remarks=? where taker_id=?");
			$stmt->bind_param("sss", $result, $remarks, $id);
				if($stmt->execute()){
					return 1;
				}else{
					return 0;
				}
		}
		public function viewExambyID($id){
			$stmt = $this->con->prepare("select * from exam where exam_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();
		}	
	}
?>
