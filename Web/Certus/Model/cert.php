<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class cert{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		public function createCert($prof_id, $cert_name, $cert_lvl, $provider_name, $cert_add, $cert_ach, $cert_from, $cert_to, $cert_pic, $stat){
			$stmt = $this->con->prepare("insert into cert (prof_id, cert_name, cert_lvl, provider_name, cert_add, cert_ach, cert_from, cert_to, cert_pic, cert_status) values (?,?,?,?,?,?,?,?,?,?)");
			$stmt->bind_param("ssssssssss", $prof_id, $cert_name, $cert_lvl, $provider_name, $cert_add, $cert_ach, $cert_from, $cert_to, $cert_pic, $stat);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function editCert($cert_name, $cert_lvl, $cert_ach, $cert_from, $cert_to, $cert_id, $provider_name){
			$stmt = $this->con->prepare("update cert set cert_name=?, cert_lvl=?, cert_ach=?, cert_from=?, cert_to=?, provider_name=? where cert_id=?");
			$stmt->bind_param("sssssss", $cert_name, $cert_lvl, $cert_ach, $cert_from, $cert_to, $provider_name, $cert_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function deleteCert($id){
			$stmt = $this->con->prepare("delete from cert where cert_id=?");
			$stmt->bind_param("s", $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
		public function viewAllCert($id){
			$stmt = $this->con->prepare("select * from cert where prof_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($cert_id, $prof_id, $provider_name, $cert_name, $cert_lvl, $cert_add, $cert_ach, $cert_from, $cert_to, $cert_pic, $cert_stat);
			$cert=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['cert_id'] = $cert_id;
					$temp['prof_id'] = $prof_id;
					$temp['provider_name'] = $provider_name;
					$temp['cert_name'] = $cert_name;
					$temp['cert_lvl'] = $cert_lvl;
					$temp['cert_add'] = $cert_add;
					$temp['cert_ach'] = $cert_ach;
					$temp['cert_from'] = $cert_from;
					$temp['cert_to'] = $cert_to;
					$temp['cert_pic'] = $cert_pic;
					$temp['cert_stat'] = $cert_stat;
					array_push($cert, $temp);
				}
			return $cert;	
		}
		public function viewExpired($id){
			$ex=array();
			$stmt = $this->con->prepare("select cert_id, cert_to, cert_status from cert where prof_id=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $exp, $stat);
			while($stmt->fetch()){
				if(strtotime($exp)<=strtotime(date('y-m-d'))){
					if($stat!="Expired"){
						$temp=array();
						$temp['id']=$id;
						array_push($ex, $temp);
					}
				}
			}
			return $ex;
		}
		public function changeStatus($id, $stat){
			$stmt=$this->con->prepare("update cert set cert_status=? where cert_id=?");
			$stmt->bind_param("ss",$stat, $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}
	}
?>