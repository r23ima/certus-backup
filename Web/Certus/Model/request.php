<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
	class request{
		private $con;
		
		function __construct(){
			
			require_once dirname(__FILE__).'/db_connection.php';
			
			$db = new DbConnect();
			
			$this->con = $db->connect();
		}
		
		public function createRequest($prof_id, $prov_id, $paytype_id, $date, $descr, $total, $status){
			$stmt = $this->con->prepare("insert into request (prof_id, prov_id, paytype_id, date, descr, total, status) values (?,?,?,?,?,?,?)");
			$stmt->bind_param("sssssss", $prof_id, $prov_id, $paytype_id, $date, $descr, $total, $status);
				if($stmt->execute()){
					$last_id = $this->con->insert_id;
					return $last_id;
				}
				else
					return 0;
		}

		public function sendMessage($message, $request_id){
			$stmt = $this->con->prepare("update request set message=? where request_id=?");
			$stmt->bind_param("ss", $message, $request_id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}

		public function updateRequest($stat, $id){
			$stmt = $this->con->prepare("update request set status=? where request_id=?");
			$stmt->bind_param("ss", $stat, $id);
				if($stmt->execute())
					return 1;
				else
					return 0;
		}

		public function createTaker($request_id, $exam_id){
			$stmt = $this->con->prepare("insert into taker (request_id, exam_id) values (?,?)");
			$stmt->bind_param("ss", $request_id, $exam_id);
				if($stmt->execute())
					return 1;
				else
					return 0;	
		}
		
		public function viewSingleRequest($id){
			$stmt = $this->con->prepare(
				"select request_id, date, prof_user, concat(prof_first, ' ', prof_last) as fullname,
				 prof_pic, prof_addr, prof_email, descr, request.status, 
				 year(CURDATE()) - year(prof_birth) as age, pay_type.name, total, discount from request

				 inner join prof on prof.prof_id = request.prof_id 

				 inner join pay_type on pay_type.paytype_id = request.paytype_id

				 where request_id=? limit 1
				");

			$stmt->bind_param("s", $id);
			$stmt->execute();
			return $stmt->get_result()->fetch_assoc();	
		}

		public function viewCourses($id){
			$stmt = $this->con->prepare("
				select exam.exam_id, name, lvl, rate, remarks, result
				from exam inner join taker
				on exam.exam_id = taker.exam_id
				inner join request
				on taker.request_id = request.request_id
				where taker.request_id = ?
				");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($id, $name, $lvl, $rate, $remarks, $result);
			$courses=array();
				while($stmt->fetch()){
					$temp=array();
					$temp['id'] = $id;
					$temp['name'] = $name;
					$temp['lvl'] = $lvl;
					$temp['rate'] = $rate;
					$temp['remarks'] = $remarks;
					$temp['result'] = $result;
					array_push($courses, $temp);
				}
			return $courses;	
		}

	}
?>