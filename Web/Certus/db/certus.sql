-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2019 at 08:03 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `certus`
--

-- --------------------------------------------------------

--
-- Table structure for table `cert`
--

CREATE TABLE `cert` (
  `cert_id` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `provider_name` text NOT NULL,
  `cert_name` text NOT NULL,
  `cert_lvl` varchar(1) NOT NULL,
  `cert_add` date NOT NULL,
  `cert_ach` date NOT NULL,
  `cert_from` date NOT NULL,
  `cert_to` date NOT NULL,
  `cert_pic` text NOT NULL,
  `cert_status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cert`
--

INSERT INTO `cert` (`cert_id`, `prof_id`, `provider_name`, `cert_name`, `cert_lvl`, `cert_add`, `cert_ach`, `cert_from`, `cert_to`, `cert_pic`, `cert_status`) VALUES
(1, 2, 'Maxwell Prime', 'AC Mechanic', '3', '2019-06-20', '2019-06-24', '2019-06-03', '2019-07-01', 'AC Mechanic-2.jpeg', 'Expired'),
(2, 2, 'Fhar Academy', 'Python Programming', '2', '2019-06-21', '2019-03-01', '2019-03-01', '2019-06-21', 'sdsdfsdf-2.jpeg', 'Expired');

-- --------------------------------------------------------

--
-- Table structure for table `educ`
--

CREATE TABLE `educ` (
  `educ_id` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `school` text NOT NULL,
  `degree` text NOT NULL,
  `educ_start` date NOT NULL,
  `educ_end` date NOT NULL,
  `educ_award` text NOT NULL,
  `educ_lvl` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `educ`
--

INSERT INTO `educ` (`educ_id`, `prof_id`, `school`, `degree`, `educ_start`, `educ_end`, `educ_award`, `educ_lvl`) VALUES
(2, 2, 'University of Cebu', 'Secondary Education', '2019-06-22', '2019-11-01', 'Most loyal to MAPEH! Yeah rockNroll', 'High School');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `exam_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `descr` text NOT NULL,
  `lvl` text NOT NULL,
  `slots` varchar(10) NOT NULL,
  `rate` varchar(100) NOT NULL,
  `prerequisit` text NOT NULL,
  `duration` varchar(50) NOT NULL,
  `req_doc` text NOT NULL,
  `prov_id` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `valid_to` date NOT NULL,
  `valid_from` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`exam_id`, `name`, `descr`, `lvl`, `slots`, `rate`, `prerequisit`, `duration`, `req_doc`, `prov_id`, `status`, `valid_to`, `valid_from`) VALUES
(1, 'Certification for Baking', 'The courseware under this course category are:\r\n\r\nPreparing Cakes (under development)\r\nPreparing Bakery Product (under development)\r\nPreparing Petits Fours (under development)\r\nPreparing Pastry Products (under development)', 'NCII', '25', '10000', 'Note that the prerequisite in taking this course is a National Certificate (NC) of any qualification.', '2 Days', 'Submit the following documentary requirements:\r\n1. Duly accomplished Application Form;\r\n2. Properly and completely filled-out  Self Assessment Guide of your chosen qualification;\r\n3. Three (3) pieces of colored and passport size picture, white background, with collar and with name printer at the back;', 1, 'ACTIVE', '0000-00-00', '0000-00-00'),
(2, 'Angularjs Junior Developer Jumpstart', 'What you''ll learn Learn the benefits of Single Page Applications Learn and be able to explain the key components in Angular Stay up-to-date on Angular with Google Developer Expert (GDE) Dan Wahlin Teach HTML New Tricks with Directives Build Controllers and Bind Data to Views Use $scope for Data Binding Understand the importance of two-way data binding Understand the Role of Modules Learn what routing is and why it''s so important in Angular apps Understand how to use Factories and Services Learn how to "Jazz" up your SPAs with Animations', 'NCIII', '30', '5000', 'Web developers with existing experience working with HTML, CSS, and JavaScript', '2 Days', 'Any text editor can be used to follow along and build AngularJS code.', 1, 'ACTIVE', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pay_type`
--

CREATE TABLE `pay_type` (
  `paytype_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `type` text NOT NULL,
  `discount` varchar(15) NOT NULL,
  `prov_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pay_type`
--

INSERT INTO `pay_type` (`paytype_id`, `name`, `type`, `discount`, `prov_id`) VALUES
(1, 'HQ Payment Plan', 'Bi-Weekly', '2', 1),
(3, 'Training Session', 'Full', '5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prof`
--

CREATE TABLE `prof` (
  `prof_id` int(11) NOT NULL,
  `prof_last` varchar(50) NOT NULL,
  `prof_first` varchar(50) NOT NULL,
  `prof_birth` date NOT NULL,
  `prof_gender` varchar(6) NOT NULL,
  `prof_addr` text NOT NULL,
  `prof_email` varchar(50) NOT NULL,
  `prof_user` varchar(50) NOT NULL,
  `prof_pass` varchar(32) NOT NULL,
  `prof_stat` varchar(15) NOT NULL,
  `prof_pic` text NOT NULL,
  `prof_lat` varchar(20) NOT NULL,
  `prof_lon` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prof`
--

INSERT INTO `prof` (`prof_id`, `prof_last`, `prof_first`, `prof_birth`, `prof_gender`, `prof_addr`, `prof_email`, `prof_user`, `prof_pass`, `prof_stat`, `prof_pic`, `prof_lat`, `prof_lon`) VALUES
(1, 'Vlaq', 'Lucian', '1995-08-05', 'Male', 'Gorordo Ave, Cebu City, Cebu', 'lucian@gmail.com', 'lucian', 'f2dfccc32edc5559b624f1a007a6cbd5', 'Active', 'lucian.jpg', '10.321234', '123.898308'),
(2, 'Montayre', 'Editha', '1962-06-20', 'Female', 'Purok 8 Camputhaw, Cebu City Cebu, 6000 Philippines. ', 'montayree@gmail.com', 'editha', 'f1ff7f5c1f6242c73a32e424025ea937', 'Active', 'editha.jpeg', '10.320042', '123.896849');

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `prov_id` int(11) NOT NULL,
  `prov_name` varchar(50) NOT NULL,
  `prov_addr` text NOT NULL,
  `prov_email` varchar(50) NOT NULL,
  `prov_user` varchar(50) NOT NULL,
  `prov_pass` varchar(32) NOT NULL,
  `prov_pic` text NOT NULL,
  `prov_lat` varchar(20) NOT NULL,
  `prov_lon` varchar(20) NOT NULL,
  `prov_stat` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`prov_id`, `prov_name`, `prov_addr`, `prov_email`, `prov_user`, `prov_pass`, `prov_pic`, `prov_lat`, `prov_lon`, `prov_stat`) VALUES
(1, '3A PRIME HOSPITALITY', '2, DG3 building, 72 N Escario St, Cebu City, 6000 Cebu', '3aprime@gmail.com', 'prime', '10f12151ef41361b91ee3fc1065e14d2', 'prime.png', '10.316257', '123.892503', 'Active'),
(11, 'canon', 'canon', 'canon@gmail.com', 'canon', '6638070bc99321b316adbd95e6951f5b', 'canon.jpeg', '10.290237', '123.879216', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `request_id` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `prov_id` int(11) NOT NULL,
  `paytype_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `descr` text NOT NULL,
  `message` varchar(200) NOT NULL,
  `total` varchar(100) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`request_id`, `prof_id`, `prov_id`, `paytype_id`, `date`, `descr`, `message`, `total`, `status`) VALUES
(7, 2, 1, 3, '2019-08-11', '', '', '5000.0', 'ONGOING'),
(8, 2, 1, 1, '2019-08-18', '', '', '10000.0', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `sched`
--

CREATE TABLE `sched` (
  `sched_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `valid_to` date NOT NULL,
  `valid_from` date NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sched`
--

INSERT INTO `sched` (`sched_id`, `exam_id`, `start_date`, `end_date`, `valid_to`, `valid_from`, `status`) VALUES
(1, 1, '2019-08-01', '2019-08-03', '2020-08-02', '2019-08-03', 'OPEN'),
(2, 1, '2019-09-02', '2019-09-03', '2020-09-02', '2019-09-03', 'OPEN'),
(3, 1, '2019-08-09', '2019-08-22', '2019-08-17', '2019-08-16', 'CLOSED');

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE `skill` (
  `skill_id` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `descr` text NOT NULL,
  `special` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`skill_id`, `prof_id`, `descr`, `special`) VALUES
(1, 2, 'Tailoring', 'Dress Making');

-- --------------------------------------------------------

--
-- Table structure for table `taker`
--

CREATE TABLE `taker` (
  `taker_id` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `result` text NOT NULL,
  `status` varchar(15) NOT NULL,
  `cert_file` text NOT NULL,
  `request_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taker`
--

INSERT INTO `taker` (`taker_id`, `remarks`, `result`, `status`, `cert_file`, `request_id`, `exam_id`) VALUES
(6, '', '', '', '', 7, 2),
(7, '', '', '', '', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback`
--

CREATE TABLE `tbl_feedback` (
  `feedback_id` int(11) NOT NULL,
  `feedback_date` date NOT NULL,
  `feedback_rating` varchar(1) NOT NULL,
  `feedback_review` text NOT NULL,
  `prov_id` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_feedback`
--

INSERT INTO `tbl_feedback` (`feedback_id`, `feedback_date`, `feedback_rating`, `feedback_review`, `prov_id`, `prof_id`) VALUES
(1, '2019-07-12', '4', 'nice', 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cert`
--
ALTER TABLE `cert`
  ADD PRIMARY KEY (`cert_id`),
  ADD KEY `prof_id` (`prof_id`);

--
-- Indexes for table `educ`
--
ALTER TABLE `educ`
  ADD PRIMARY KEY (`educ_id`),
  ADD KEY `prof_id` (`prof_id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`exam_id`),
  ADD KEY `prov_id` (`prov_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `request_id` (`request_id`);

--
-- Indexes for table `pay_type`
--
ALTER TABLE `pay_type`
  ADD PRIMARY KEY (`paytype_id`),
  ADD KEY `prov_id` (`prov_id`);

--
-- Indexes for table `prof`
--
ALTER TABLE `prof`
  ADD PRIMARY KEY (`prof_id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`prov_id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `prof_id` (`prof_id`),
  ADD KEY `prov_id` (`prov_id`),
  ADD KEY `pay_id` (`paytype_id`);

--
-- Indexes for table `sched`
--
ALTER TABLE `sched`
  ADD PRIMARY KEY (`sched_id`),
  ADD KEY `exam_id` (`exam_id`);

--
-- Indexes for table `skill`
--
ALTER TABLE `skill`
  ADD PRIMARY KEY (`skill_id`),
  ADD KEY `prof_id` (`prof_id`);

--
-- Indexes for table `taker`
--
ALTER TABLE `taker`
  ADD PRIMARY KEY (`taker_id`),
  ADD KEY `request_id` (`request_id`),
  ADD KEY `exam_id` (`exam_id`);

--
-- Indexes for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  ADD PRIMARY KEY (`feedback_id`),
  ADD KEY `prof_id` (`prof_id`),
  ADD KEY `prov_id` (`prov_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cert`
--
ALTER TABLE `cert`
  MODIFY `cert_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `educ`
--
ALTER TABLE `educ`
  MODIFY `educ_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pay_type`
--
ALTER TABLE `pay_type`
  MODIFY `paytype_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `prof`
--
ALTER TABLE `prof`
  MODIFY `prof_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `prov_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sched`
--
ALTER TABLE `sched`
  MODIFY `sched_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `skill`
--
ALTER TABLE `skill`
  MODIFY `skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taker`
--
ALTER TABLE `taker`
  MODIFY `taker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
