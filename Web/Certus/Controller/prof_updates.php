<?php
	require_once '../model/prof.php';
	$response = array();
	$db = new prof();
	if(isset($_POST['id'])){
		if($_POST['char']=='d'){
			$res=$db->deleteProf($_POST['id'], "Deactivated");
				if($res==1){
					$response['error'] = false;
					$response['message'] = "Account deactivated succesfully";
				}else{
					$response['error'] = true;
					$response['message'] = "Something went wrong";
				}
		}else{
			$res=$db->editProf($_POST['last'], $_POST['first'], $_POST['addr'], $_POST['id']);
				if($res==1){
					$response['error'] = false;
					$response['message'] = "Profile succesfully updated";
				}else{
					$response['error'] = true;
					$response['message'] = "Something went wrong";
				}
		}
	}

	echo json_encode($response);
?>