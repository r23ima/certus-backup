<?php
require_once '../model/feedback.php';
$response = array();
	if($_SERVER['REQUEST_METHOD']=='POST'){
		if(isset($_POST['username']))
		{
			$db = new feedbacks();
			$date=date('y-m-d');
		$result = $db->createFeedback($date, $_POST['rating'], $_POST['review'],
		$_POST['customer'], $_POST['provider']);
			if($result==1){
				$response['error'] = false;
				$response['message'] = "Feedback created successfully";
			}
			elseif($result==2){
				$response['error'] = true;
				$response['message'] = "Some error occured. Please try again";
			}
			elseif($result==0){
				$response['error'] = true;
				$response['message'] = "No transaction yet";
			}
		
		}
		else{
			$response['error'] = true;
			$response['message'] = "Required Fields are missing";
		}
	}
	else{
		$reponse['error'] = true;
		$response['message'] = "Invalid Request";
	}
	
	echo json_encode($response);
?>