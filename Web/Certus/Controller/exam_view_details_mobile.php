<?php
	require_once '../model/exam.php';
	$db = new exam();
	$response = array();
	$e = $db->viewExambyID($_POST['id']);
		$response['exam_id'] 	=$e['exam_id'];
		$response['name']    	=$e['name'];
		$response['descr']   	=$e['descr'];
		$response['lvl']     	=$e['lvl'];
		$response['slots']   	=$e['slots'];
		$response['rate']    	=$e['rate'];
		$response['pre']     	=$e['prerequisit'];
		$response['to']		    =$e['valid_to'];
		$response['from'] 		=$e['valid_from'];
		$response['duration']   =$e['duration'];
		$response['req_doc']    =$e['req_doc'];
		$response['prov_id']    =$e['prov_id'];
	echo json_encode($response);
?>