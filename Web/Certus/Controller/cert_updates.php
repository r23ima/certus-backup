<?php
	require_once '../model/cert.php';
	$response = array();
	$db = new cert();
	if($_POST['char']=='d'){
		$res=$db->deleteCert($_POST['id']);
				if($res==1){
					$response['error'] = false;
					$response['message'] = "Deleted Successfully";
				}else{
					$response['error'] = true;
					$response['message'] = "Some error occured";
				}
	}
	elseif($_POST['char']=='ex'){
		$res=$db->changeStatus($_POST['id'], "Expired");
				if($res==1){
					$response['error'] = false;
					$response['message'] = "Updated Successfully";
				}else{
					$response['error'] = true;
					$response['message'] = "Some error occured";
				}
	}
	else{
		/*$certpic = $_POST['pic'];
	 	$upload_path = '../uploads/';
		$name = $_POST['cert_name']."-".$_POST['prof_id'].".jpeg";
		$path=$upload_path.$name;
		file_put_contents( $path, base64_decode($certpic));*/

		$res = $db->editCert($_POST['cert_name'], 
			$_POST['cert_lvl'], 
			date('y-m-d', strtotime($_POST['cert_ach'])), 
			date('y-m-d', strtotime($_POST['cert_from'])),
			date('y-m-d', strtotime($_POST['cert_to'])), 
			$_POST['cert_id'],
			$_POST['provider_name']);
				if($res==1){
					$response['error'] = false;
					$response['message'] = "Updated Successfully";
				}else{
					$response['error'] = true;
					$response['message'] = "Some error occured";
				}
	}
		echo json_encode($response);
?>