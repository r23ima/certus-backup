<?php
	require_once '../model/educ.php';
	$response = array();
	$db = new educ();
		if($_POST['char']=="d"){
			$res=$db->deleteEduc($_POST['id']);
			if($res==1){
				$response['error'] = false;
				$response['message'] = "Deleted Successfully";
			}else{
				$response['error'] = true;
				$response['message'] = "Some error occured";
			}
		}else{
			$res = $db->editEduc($_POST['school'], date('y-m-d', strtotime($_POST['educ_start'])), date('y-m-d', strtotime($_POST['educ_end'])), $_POST['educ_award'],  $_POST['degree'],  $_POST['id']);

			if($res==1){
				$response['error'] = false;
				$response['message'] = "Updated Successfully";
			}else{
				$response['error'] = true;
				$response['message'] = "Some error occured";
			}
		}	
	echo json_encode($response);
?>