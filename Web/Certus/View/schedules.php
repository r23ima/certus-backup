<!DOCTYPE html>
<html lang="en">
<head>
  <title>Schedules</title>
	
	<?php
		include("csslinks.php");
		if(isset($_GET['message'])){
	        $m = $_GET['message'];
	        echo "<script>alert("; 
	          echo $m;
	        echo ")</script>";
	    }
	?>
</head>
<body>

	<?php
		include("header_main.php");
		require_once '../model/sched.php';
		$db = new sched();
		$res = $db->viewAllSched($_GET['id'], '');
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item active">
                                       <a href="courses.php">Courses</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Exam Schedules</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" placeholder="Search..." id="search" aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Exam Schedules table</h3>
                            <div><br/>
		                        <button class="fas fa-plus btn btn-primary" style="float:left;" data-role='add'> &nbsp;Add Schedules</button>
                            </div>
                            <div class="table-responsive">
                            	<br/>
                                <table class="table">
                                    <thead>
                                        <tr>   
											<th>ID</th>
                                            <th>Course Start</th>
                                            <th>Course End</th>
											<th>Certification Valid From</th>
											<th>Certification Valid To</th>
											<th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    		for($i=0; $i<count($res); $i++){
		                                        echo "<tr>
		                                            <td>{$res[$i]['sched_id']}</td>
													<td>{$res[$i]['start']}</td>
		                                            <td>{$res[$i]['end']}</td>   
		                                            <td>{$res[$i]['from']}</td>
		                                            <td>{$res[$i]['to']}</td>
		                                            <td>{$res[$i]['stat']}</td>
		                                        </tr>"; 
		                                    }                       
                                    	?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
  
		<script>
			$(document).ready(function(){
				$(document).on('click', 'button[data-role=edit]', function(){
					var id=$(this).closest('tr').attr('id');
					var sp=$('#'+id).children('td[data-target=sp_id]').text();
			
					$('#sp_id').val(sp);
					$('#mediumModal').modal('toggle');
				});			
			});
		</script>
			<!-- The Modal -->
		  <div class="modal fade" id="mediumModal">
			<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title">Add Schedules</h4>
				  <button type="button" class="close" data-dismiss="modal">×</button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				 <form method="POST" action="../controller/sched_add.php" id="myForm">
					 <input type='hidden' name='exam_id' value="<?php echo $_GET['id']; ?>">
					 <label>Start Date</label>  
	                	<input type="date" class="form-control" name="start_date" required>
	                <label>End Date</label> 
	                	<input type="date" class="form-control" name="end_date" required>
	                <label>Valid From</label>  <small>(Certificate validity from)</small>
	                	<input type="date" class="form-control" name="valid_from" required>
	                <label>Valid To</label>  <small>(Certificate validity to)</small>
	                	<input type="date" class="form-control" name="valid_to" required>
	                <label>Status</label>
	                	<select name="stat" class="form-control">
	                		<option value="OPEN">OPEN</option>
	                		<option value="CLOSED">CLOSED</option>
	                		<option value="ONGOING">ONGOING</option>
	                	</select>
	                
				</div>
				
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">Save</button>
				  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
				</form>
			  </div>
			</div>
		  </div>
		  <script>
		    $(document).ready(function(){
		      $(document).on('click', 'button[data-role=add]', function(){    
		        $('#mediumModal').modal('toggle');
		      });
		      $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

          $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
              var id = $(this).text().toLowerCase().trim();
              var not_found = (id.indexOf(value) == -1);
              $(this).closest('tr').toggle(!not_found);
              return not_found;
            });
          });
        });   
		    });
		  </script>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
