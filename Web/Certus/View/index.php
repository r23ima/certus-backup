<!DOCTYPE html>
<html lang="en">

<head>

  <title>Certus</title>

  <?php
  session_start();
    if(isset($_SESSION['id'])){
      header("Location:dashboard.php");
      exit;
    }

	 include("csslinks.php");
  ?>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-light bg-light static-top">
    <div class="container">
      <a class="navbar-brand" href="#"><img src="img/certus.png" style="height: 60px; width: 100px;"></a>
		  <form class="form-inline">
			<button class="btn form-control" type="button" style="background-color: darkblue;"><a style="color: gold;" href="../view/login.php">Sign in</a></button>
			<button class="btn form-control" type="button" style="background-color: darkblue;"><a style="color: gold;" href="../view/register.php">Register</a></button>
		  </form>
	</div>
  </nav>

  <!-- Masthead -->
  <header class="masthead text-center" style="position: relative;
  background-color: #363e44;
  color: gold;">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-9 mx-auto">
          <h1 class="mb-5">CERTUS: AN ONLINE PLATFORM FOR PROFESSIONAL CERTIFICATION MANAGEMENT AND MONITORING</h1>
        </div>
      </div>
    </div>
  </header>


  <!-- Image Showcases -->
  <section class="showcase">
    <div class="container-fluid p-0">
      <div class="row no-gutters">

        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/cal.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
          <h2>Certificate Tracker</h2>
          <p class="lead mb-0">When your busy with your everyday life, Certus app is a great app to notify you and keep tract of your certificate's validity.</p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/map.jpg');"></div>
        <div class="col-lg-6 my-auto showcase-text">
          <h2>Map Integration</h2>
          <p class="lead mb-0">See schools and institutions nearby and check out their courses offered and feedbacks and ratings.</p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/bg-showcase-3.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
          <h2>Easy to Use</h2>
          <p class="lead mb-0">Easy app navigation and can enroll to multiple or single courses.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Testimonials -->
  <section class="testimonials bg-light">
    <div class="container">
      <h2 class="mb-5">Objectives</h2>
        <ul class="d-flex flex-column mb-3">
          <li>Allows the professionals to manage and monitor their portfolios</li>
          <li>
            Provide access for professionals to various certification examinations relevant to profession, skills and/or interests
          </li>
          <li>Create a platform for certifying agencies to promote their establishments, manage and monitor certification exams, exam takers and passers</li>
          <li>
            Build administrative modules for managing and monitoring of subscriptions and users, and generating reports
          </li>
        </ul>
    </div>
  </section>

	  <?php
		include("footer_main.php");
      if(isset($_GET['message'])){
        $m = $_GET['message'];
        echo "<script>alert("; 
          echo $m;
        echo ")</script>";
      }
	  ?>
</body>

</html>
