<!DOCTYPE html>
<html lang="en">
<head>
  <title>Transaction</title>
	
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
    require_once '../model/transaction.php';

    if(isset($_GET['message'])){
          $m = $_GET['message'];
          echo "<script>alert("; 
              echo $m;
          echo ")</script>";
      }
      $db = new trans();
      $res = $db->viewAllTransaction($_SESSION['id']);
      $cc=count($res);
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Transaction</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" id="search" placeholder="Search..." aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Transaction table</h3>
                            <div>
                                <div>
                                    <div class="dropdown" style="float: left;">
                                        <button class="fas fa-filter btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> &nbsp;Sort By
                                        </button>
                                        <ul class="dropdown-menu">
                                         <li><a class="dropdown-item" href="#">Done</a></li>
                                          <li><a class="dropdown-item" href="#">Pending</a></li>
                                          <li><a class="dropdown-item" href="#">Finish</a></li>
                                          <li><a class="dropdown-item" href="#">Cancelled</a></li>
                                        </ul>
                                    </div>
                                </div>    
                            </div>
                            <div class="table-responsive">
                                <br/>
                                <table class="table">
                                    <thead>
                                        <tr>   
											                      <th>Transaction Number</th>
                                            <th>Date Requested</th>
                                            <th>Name</th>
                                            <th>Total</th>
                                            <th>Payment</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                        for($i=0; $i<$cc; $i++){
                                           echo"<tr>
                                                <td>
                                                  <a href='request_details.php?id={$res[$i]['id']}'>{$res[$i]['id']}</a>
                                                </td>
    											                      <td>{$res[$i]['date']}</td>
                                                <td>{$res[$i]['full']}</td>
                                                <td>{$res[$i]['total']}</td>
                                                <td><a href='payment_details.php?id={$res[$i]['id']}'>See Details</a></td>";
    											                      if($res[$i]['stat']=="DECLINE"){
                                                  echo "<td>
                                                      <span class='badge badge-danger'>{$res[$i]['stat']}</span>
                                                  </td>";
                                                }
                                                elseif($res[$i]['stat']=="COMPLETED" || $res[$i]['stat']=="DONE" || $res[$i]['stat']=="APPROVE"){
                                                  echo "<td>
                                                      <span class='badge badge-success'>{$res[$i]['stat']}</span>
                                                  </td>";
                                                }
                                                elseif($res[$i]['stat']=="ONGOING"){
                                                  echo "<td>
                                                      <span class='badge badge-primary'>{$res[$i]['stat']}</span>
                                                  </td>";
                                                }
                                                elseif($res[$i]['stat']=="PENDING"){
                                                  echo "<td>
                                                      <span class='badge badge-warning'>{$res[$i]['stat']}</span>
                                                  </td>";
                                                }
                                                elseif($res[$i]['stat']=="CANCELLED"){
                                                  echo "<td>
                                                      <span class='badge badge-success'>{$res[$i]['stat']}</span>
                                                  </td>";
                                                }
                                                   
                                            echo "</tr>";
                                        }                          
                                      ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
<script>
      $(document).ready(function(){
        $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

          $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
              var id = $(this).text().toLowerCase().trim();
              var not_found = (id.indexOf(value) == -1);
              $(this).closest('tr').toggle(!not_found);
              return not_found;
            });
          });
        });     
      });
    </script>
	<?php
		include("footer_main.php");
	?>

</body>
</html>
