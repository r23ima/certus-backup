<!DOCTYPE html>
<html lang="en">
<head>
  <title>Courses</title>
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
    require_once '../model/exam.php';
    $db = new exam();
    $examArr = $db->viewAllExam($_SESSION['id']);
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2" ng-app="app" ng-controller="ctrl">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Courses</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" placeholder="Search..." id="search" aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Certification Exam table</h3>    
                            </div><br/>
                            <button class="fas fa-plus btn btn-primary" style="float:left;" data-role='add'> &nbsp;Add Exam</button>&nbsp;
                              <div class="dropdown">
                                <button class="fas fa-filter btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> &nbsp;Sort By
                                </button>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="#">Status</a></li>
                                  <li><a class="dropdown-item" href="#">Number of Students</a></li>
                                  <li><a class="dropdown-item" href="#">Category</a></li>
                                  <li><a class="dropdown-item" href="#">Level</a></li>
                                </ul>
                              </div>
                            <div class="table-responsive">
                              <br/>
                                <table class="table">
                                    <thead>
                                        <tr>   
											                      <th>Exam ID</th>
                                            <th>Name</th>
                                            <th>Level</th>
                                            <th>Slots</th>
                                            <th>Rate</th>
                                            <th>Students</th>
                                            <th>Schedules</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php
                                    for($i=0; $i<count($examArr); $i++){
                                      echo"
                                        <tr id='{$i}'>
                                           <td data-target='ex_id'>
                                                <a href='exam_details.php?id={$examArr[$i]['exam_id']}'>{$examArr[$i]['exam_id']}
                                                </a>
                                           </td>
											                     <td data-target='name'>{$examArr[$i]['name']}</td>
                                           <td data-target='lvl'>{$examArr[$i]['lvl']}</td>
                                           <td data-target='slots'>{$examArr[$i]['slots']}</td>
                                           <td data-target='rate'>{$examArr[$i]['rate']}</td>
                                           <td data-target='to'>
                                            <a href='students.php?id={$examArr[$i]['exam_id']}'>See Students
                                                </a>
                                           </td>
                                           <td data-target='to'>
                                            <a href='schedules.php?id={$examArr[$i]['exam_id']}'>See Schedules
                                                </a>
                                           </td>";

                                            if($examArr[$i]['stat']=="ACTIVE"){
                                                  echo "<td>
                                                      <span class='badge badge-success'>{$examArr[$i]['stat']}</span>
                                                  </td>";
                                            }
                                            elseif($examArr[$i]['stat']=="DEACTIVATE"){
                                                  echo "<td>
                                                      <span class='badge badge-danger'>{$examArr[$i]['stat']}</span>
                                                  </td>";
                                            }

                                        echo "</tr>";                       
                                    ;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
  <!-- The Modal -->
  <div class="modal fade" id="mediumModal2">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Add Exam</h5>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
         <div class="modal-body">
          <form action="../controller/exam_add.php" method="POST">
              <input type="hidden" name="prov_id" value="<?php echo $_SESSION['id']; ?>">
              <label>Name</label>  
                <input type="text" class="form-control" name="name" placeholder="Name" required>
              <label>Description</label>  
                <input type="text" class="form-control" name="descr" placeholder="Description" required>
              <label>Level</label> <small>(ex. NCII, NCIV)</small>
                <input type="text" class="form-control" name="lvl" placeholder="Level" required>
              <label>Slots</label>   
                <input type="text" class="form-control" name="slots" placeholder="Slots" required>
              <label>Rate</label> <small>(per student)</small>
                <input type="Number" class="form-control" name="rate" placeholder="Rate" required>
              <label>Prerequisite</label>   
                <textarea class="form-control" name="pre" placeholder="Prerequisits" required></textarea>
              <label>Valid From</label>
              <input type="date" class="form-control" name="valid_from" placeholder="Slots" required>
              <label>Valid To</label>
              <input type="date" class="form-control" name="valid_to" placeholder="Slots" required>
              <label>Duration</label>   
                <input type="text" class="form-control" name="duration" placeholder="Duration"required>
              <label>Required Documents</label>
                <textarea class="form-control" name="req_doc" placeholder="Required Documents"required></textarea>
          </div>    
        <!-- Modal footer -->
        <div class="modal-footer">
              <button type="submit" class="btn btn-danger">Submit</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function(){
      $(document).on('click', 'button[data-role=add]', function(){    
        $('#mediumModal2').modal('toggle');
      });
      $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

          $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
              var id = $(this).text().toLowerCase().trim();
              var not_found = (id.indexOf(value) == -1);
              $(this).closest('tr').toggle(!not_found);
              return not_found;
            });
          });
        }); 
    });
  </script>
	<?php
		include("footer_main.php");
	?>
</body>
</html>
