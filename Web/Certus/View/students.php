<!DOCTYPE html>
<html lang="en">
<head>
  <title>Students</title>
	
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
		if(isset($_GET['id'])){
			$id = $_GET['id'];
		}
		if(isset($_GET['message'])){
	        $m = $_GET['message'];
	        echo "<script>alert("; 
	          echo $m;
	        echo ")</script>";
	    }
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item active">
                                       <a href="courses.php">Courses</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Students</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" id="search" placeholder="Search..." aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Student table</h3>
                            <div><br/>
	                            <button class="fas fa-filter btn btn-primary dropdown-toggle" type="button" style="float:left;" data-toggle="dropdown"> &nbsp;Sort By
	                            </button>
	                                <ul class="dropdown-menu">
	                                  <li><a class="dropdown-item" href="#">Name</a></li>
	                                  <li><a class="dropdown-item" href="#">ID</a></li>
	                                </ul>
                            </div>
                            <div class="table-responsive">
                            	<br/>
                                <table class="table">
                                    <thead>
                                        <tr>   
											<th>ID</th>
                                            <th>Name</th>
                                            <th>Remarks</th>
											<th>Result</th>
											<th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    		require_once '../model/exam.php';
											$db = new exam();
											$res = $db->viewAllStudentsByExam($id);
                               				
                               				for($i=0; $i<count($res); $i++){
                               					if($res[$i]['remarks']=='')
                               						$r1 = "N/A";
                               					else
                               						$r1 = $res[$i]['remarks'];

                               					if($res[$i]['result']=='')
                               						$r2 = "N/A";
                               					else
                               						$r2 = $res[$i]['result'];

		                                        echo "<tr id='{$i}'>
		                                            <td data-target='id'>{$res[$i]['taker_id']}</td>
													<td>{$res[$i]['fullname']}</td>
		                                            <td>{$r1}</td>   
		                                            <td>{$r2}</td>									
													<td>
														<div class='row'>
															<div class='col'>
																<button type='submit' class='btn btn-outline-warning btn-sm btn-block' data-role='evaluate'>Evaluate <i class='fas fa-percentage'></i></button>
															</div>
															
		                                                </div>
													</td>
		                                        </tr>";
		                                    }                            
                                    	?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
  
		<script>
			$(document).ready(function(){
				$(document).on('click', 'button[data-role=evaluate]', function(){
					var id=$(this).closest('tr').attr('id');
					var taker=$('#'+id).children('td[data-target=id]').text();
					$('#takerid').val(taker);

					$('#mediumModal').modal('toggle');
				});
				$("#search").keyup(function () {
				var value = this.value.toLowerCase().trim();

					$("table tr").each(function (index) {
						if (!index) return;
						$(this).find("td").each(function () {
							var id = $(this).text().toLowerCase().trim();
							var not_found = (id.indexOf(value) == -1);
							$(this).closest('tr').toggle(!not_found);
							return not_found;
						});
					});
				});			
			});
		</script>
			<!-- The Modal -->
		  <div class="modal fade" id="mediumModal">
			<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title">Evaluate Student</h4>
				  <button type="button" class="close" data-dismiss="modal">×</button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				 <form method="POST" action="../controller/grade_student.php" id="myForm">
				 <input type='hidden' name='id' id='takerid'>
				 <input type='hidden' name='courseid' id='<?php echo $id; ?>'>
				 <label><b>Grade of Student</b></label>
				 	<div class="row">
						<div class="col"> <input type='radio' name='grade' value='PASSED'>PASSED </div>
						<div class="col"> <input type='radio' name='grade' value='FAILED'>FAILED </div>
					</div>
					<br/>
				 <textarea name="remarks" cols="30" rows="5" class="form-control" placeholder="Write remarks...(optional)"></textarea>
				</div>
				
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">Submit</button>
				  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
				</form>
			  </div>
			</div>
		  </div>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
