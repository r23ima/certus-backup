<!DOCTYPE html>
<html lang="en">
<head>
  <title>Feedbacks and Ratings</title>
	
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
								  <li class="list-inline-item active">
                                       <a href="serviceprovider.php">Service Provider</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Feedbacks and Ratings</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center bg-light ">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Feedbacks and Ratings table</h3>
                            <br/>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>   
											<th>Feedback ID</th>
                                            <th>Date</th>
                                            <th>Ratings</th>
                                            <th>Review</th>
											<th>Customer Name</th>
                                         
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id='1'>
                                            <td data-target='rid'>0089</td>
											<td>2018-09-27</td>
                                            <td><span class='badge badge-pill badge-warning'><i class="fas fa-star"></i></span></td>
                                            <td data-target='feed'>Very Nice Service</td>    
                                            <td>Caren Cañete</td>
                                         </tr>                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
			<script>
				$(document).ready(function(){
						
				});
			</script>
	
	<?php
		include("footer_main.php");
	?>

</body>
</html>
