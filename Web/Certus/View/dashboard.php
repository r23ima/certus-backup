<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
	
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="#">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Dashboard</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" placeholder="Search..." id="search" aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Monthly Records</h3>
                        <h6 class="text-primary">For <?php echo date('F Y'); ?></h6>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>   
                  										<th>Exam | Course Name</th>
                  										<th>Number of Students</th>
                                      <th>Total Gross Earnings</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Schools</td>
										                    <td>153</td>
                                        <td>153</td>
                                    </tr>
                                    <tr>
                                        <td>Students</td>
                                        <td>153</td>
                                        <td>153</td>
                                    </tr>                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
  </section>
  <script>
      $(document).ready(function(){
        $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

          $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
              var id = $(this).text().toLowerCase().trim();
              var not_found = (id.indexOf(value) == -1);
              $(this).closest('tr').toggle(!not_found);
              return not_found;
            });
          });
        });     
      });
    </script>
	<?php
		include("footer_main.php");
	?>

</body>
</html>
