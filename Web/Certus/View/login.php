<!DOCTYPE html>
<html lang="en">

<head>

  <title>Login</title>
	
	<?php
		include("csslinks.php");
	?>

</head>

<body>

  <!-- Testimonials -->
  <section class="testimonials text-center bg-light ">
    <div class="container border">
	<br/>
	<img src="img/certus.png" style=" height: 60px; width: 100px;">
	<br/>
      <h2 class="mb-5">Login Account</h2>
      <div class="row">
             <div class="container" style="width: 80%;">
					   <form action="../controller/web_login.php" method="POST">
						<div class="form-group">
							<label for="username" style="float:left;"><b>Username</b></label>
							<input  class="form-control" type="text" name="username" placeholder="Username" required>
						</div>
						<div class="form-group">
							<label for="password" style="float:left;"><b>Password</b></label>
							<input  class="form-control" type="password" name="password" placeholder="Password" required>
						</div>
						<div class="checkbox">
						    <label>
							<input type="checkbox" name="remember">Remember Me
						   </label>
						</div>   
						   <label>
							  <a href="#">Forgotten Password?</a>
						   </label>
							<br/>
							<button class="btn btn-primary" type="submit">sign in</button>
						</form>
			 </div>
      </div>
	  <br/>
    </div>
  </section>

</body>

</html>
