<!DOCTYPE html>
<html lang="en">

<head>

  <title>Profile</title>
	
	<?php
		include("csslinks.php");
	?>

</head>

<body>
	<?php
		include("header_main.php");
		$server_ip = gethostbyname(gethostname());
		require_once '../model/users.php';
			$response = array();
			$db = new users();
				$user=$db->getProvider_details($_SESSION['id']);
				$profile_pic = 'http://'.$server_ip.'/certus/uploads/'.$user['prov_pic']; 

				if(isset($_GET['message'])){
        	$m = $_GET['message'];
        	echo "<script>alert("; 
          		echo $m;
        	echo ")</script>";
      }
	?>
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Profile</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
  <!-- Testimonials -->
  <section class="text-center bg-light ">
    <div class="container border">
	<br/>
	<img src="<?php echo $profile_pic; ?>" id="img_up" height="200" alt="Image preview..."><br/>
		<h2 class="mb-5">My Account
			<span class="badge">
				<button  class="fas fa-user-edit btn btn-primary" data-role='edit'></button> 
				<button  class="fas fa-trash btn btn-danger" data-role='del'></button>
			</span>
		</h2>
      <div class="row">
             <div class="container" style="width: 80%;">
					   <form action="../controller/prov_edit.php" method="POST">
					   	<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
						<div class="form-group">
							<label for="name" style="float:left;"><b>Name</b></label>
							<input id="name_id" class="form-control" type="text" name="name" value="<?php echo $user['prov_name']; ?>"readonly>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Address</b></label>
							<input id="addr_id" class="form-control" type="text" name="addr" readonly
							value="<?php echo $user['prov_addr']; ?>"
							>
						</div>
						<div class="form-group">
							<label for="email" style="float:left;"><b>Email</b></label>
							<input  class="form-control" type="text" name="email" readonly
							value="<?php echo $user['prov_email']; ?>"
							>
						</div>
						<div class="form-group">
							<label for="username" style="float:left;"><b>Username</b></label>
							<input  class="form-control" type="text" name="user" readonly
							value="<?php echo $user['prov_user']; ?>"
							>
						</div>
						
							<br/>
							<button class="btn btn-primary" type="submit" id='btnsave' style="display: none;">Save Changes</button>
							<button class="btn btn-primary" type="button"  id='btncancel' style="display: none;">Cancel</button>
						</form>
			 </div>
      </div>
	  <br/>
    </div>
  </section>
	<script>
		$(document).ready(function(){
			$(document).on('click', 'button[data-role=del]', function(){		
				$('#mediumModal2').modal('toggle');
			});
			$(document).on('click', 'button[data-role=edit]', function(){		
				$('#name_id').removeAttr("readonly");
				$('#addr_id').removeAttr("readonly");
				$('#btnsave').css("display", "");
				$('#btncancel').css("display", "");
			});

			$('#btncancel').click(function(){
				$('#name_id').attr("readonly", "readonly");
				$('#addr_id').attr("readonly", "readonly");
				$('#btnsave').css("display", "none");
				$('#btncancel').css("display", "none");
			});
			
		});
	</script>
	<!-- The Modal -->
  <div class="modal fade" id="mediumModal2">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Are you sure you want to deactivate this account?</h5>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        	<form action="../controller/prov_delete.php" method="POST">
        		<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>">
				<button type="submit" class="btn btn-danger">Yes</button>
	          	<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          	</form>
        </div>
      </div>
    </div>
  </div>
	<?php
		include("footer_main.php");
	?>
</body>

</html>
