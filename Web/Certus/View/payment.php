<!DOCTYPE html>
<html lang="en">
<head>
  <title>Payment</title>
	
	<?php
		include("csslinks.php");
		if(isset($_GET['message'])){
	        $m = $_GET['message'];
	        echo "<script>alert("; 
	          echo $m;
	        echo ")</script>";
	    }
	?>
</head>
<body>

	<?php
		include("header_main.php");
		require_once '../model/pay_type.php';
		$db = new paytype();
		$res = $db->viewAllPaytype($_SESSION['id']);
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Payment Type</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" placeholder="Search..." id="search" aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Payment Type table</h3>
                            <div><br/>
		                        <button class="fas fa-plus btn btn-primary" style="float:left;" data-role='add'> &nbsp;Add Payment Plan</button>
                            </div>
                            <div class="table-responsive">
                            	<br/>
                                <table class="table">
                                    <thead>
                                        <tr>   
											<th>ID</th>
                                            <th>Name</th>
                                            <th>Payment Type</th>
											<th>Discount</th>
											<th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    		for($i=0; $i<count($res); $i++){
                                    			if($res[$i]['dis']=="")
                                    				$var = "None";
                                    			else
                                    				$var = $res[$i]['dis']." %";
		                                        echo "<tr>
		                                            <td>{$res[$i]['paytype_id']}</td>
													<td>{$res[$i]['name']}</td>
		                                            <td>{$res[$i]['type']}</td>   
		                                            <td>{$var}</td>
													<td>
														<div class='table-data-feature'>
															<button class='item btn btn-primary' 
															data-toggle='tooltip' data-placement='top' title='Edit'>
		                                                        <i class='fas fa-edit'></i>
		                                                    </button>
															 <a href='../controller/paytype_delete.php?id={$res[$i]['paytype_id']}'>
															<button class='item btn btn-danger' 
															data-toggle='tooltip' data-placement='top' title='Delete'>
		                                                        <i class='fas fa-trash'></i>
		                                                    </button>
															</a>
		                                                    </button>
		                                                </div>
													</td>
		                                        </tr>"; 
		                                    }                       
                                    	?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>
  
		<script>
			$(document).ready(function(){
				$(document).on('click', 'button[data-role=edit]', function(){
					var id=$(this).closest('tr').attr('id');
					var sp=$('#'+id).children('td[data-target=sp_id]').text();
			
					$('#sp_id').val(sp);
					$('#mediumModal').modal('toggle');
				});			
			});
		</script>
			<!-- The Modal -->
		  <div class="modal fade" id="mediumModal">
			<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title">Add Payment Plan</h4>
				  <button type="button" class="close" data-dismiss="modal">×</button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				 <form method="POST" action="../controller/paytype_add.php" id="myForm">
					 <input type='hidden' name='id' value="<?php echo $_SESSION['id']; ?>">
					 <label>Name</label>  
	                	<input type="text" class="form-control" name="name" placeholder="Name" required>
	                <label>Type</label>  <small>(ex. Monthly, Full)</small>
	                	<input type="text" class="form-control" name="type" placeholder="Payment type" required>
	                <label>Discount</label>  <small>(Optional)</small>
	                	<input type="number" class="form-control" name="descr" placeholder="Percentage %"> &nbsp;
				</div>
				
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">Save</button>
				  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
				</form>
			  </div>
			</div>
		  </div>
		  <script>
		    $(document).ready(function(){
		      $(document).on('click', 'button[data-role=add]', function(){    
		        $('#mediumModal').modal('toggle');
		      });
		      $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

          $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
              var id = $(this).text().toLowerCase().trim();
              var not_found = (id.indexOf(value) == -1);
              $(this).closest('tr').toggle(!not_found);
              return not_found;
            });
          });
        });   
		    });
		  </script>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
