<!DOCTYPE html>
<html lang="en">

<head>

  <title>Register</title>
	
	<?php
		include("csslinks.php");
	?>

</head>

<body>
  <!-- Testimonials -->
  <section class="testimonials text-center bg-light ">
    <div class="container border">
	<br/>
	<img src="img/certus.png" style=" height: 60px; width: 100px;">
	<br/>
      <h2 class="mb-5">Register Account</h2>
      <div class="row">
             <div class="container" style="width: 80%;">
					<form action="../controller/prov_register.php" method="POST" enctype="multipart/form-data">
						<div class="form-group">	
							<img src="" id="img_up" height="200" alt="Image preview..."><br/><br/>
							<input type="file" id="img_btn" class="btn btn-primary" name="pic"><br/><br/>
							<label for="profile"><b>School/Institution Logo</b></label>
						</div>	<hr>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Name</b></label>
							<input  class="form-control" type="text" name="name" placeholder="Name of school/institution" required>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Address</b></label>
							<input  class="form-control" type="text" name="addr" placeholder="Address" required>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Email</b></label>
							<input  class="form-control" type="text" name="email" placeholder="Email" required>
						</div>
						<div class="form-group">
							<label for="username" style="float:left;"><b>Username</b></label>
							<input  class="form-control" type="text" name="user" placeholder="Username" required>
						</div>
						<div class="form-group">
							<label for="password" style="float:left;"><b>Password</b></label>
							<input  class="form-control" type="password" name="pass" placeholder="Password" required>
						</div>
						<div class="checkbox">
						    <label>
							<input type="checkbox" name="remember">Remember Me
						   </label>
						</div>   
						   <label>
							  <a href="#">Forgotten Password?</a>
						   </label>
							<br/>
							<button class="btn btn-primary" type="submit">Register</button>
					</form>
			 </div>
      </div>
	  <br/>
    </div>
  </section>
<script>
   function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#img_up').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
  }
}

$("#img_btn").change(function() {
  readURL(this);
});
  </script>
</body>

</html>
