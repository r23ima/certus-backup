<!DOCTYPE html>
<html lang="en">

<head>

  <title>Exam Details</title>
	
	<?php
		include("csslinks.php");
		require_once '../model/exam.php';
			$db = new exam();
				$exam=$db->viewExambyID($_GET['id']); 

		if(isset($_GET['message'])){
        	$m = $_GET['message'];
        	echo "<script>alert("; 
          		echo $m;
        	echo ")</script>";
      }
	?>

</head>

<body>
	<?php
		include("header_main.php");
	?>
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Exam</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Exam Details</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
  <!-- Testimonials -->
  <section class="text-center bg-light ">
    <div class="container border">
	<br/>
		<h2 class="mb-5">Exam Details
			<span class="badge">
				<button  class="fas fa-edit btn btn-primary" data-role='edit'></button> 
				<button  class="fas fa-trash btn btn-danger" data-role='del'></button>
			</span>
		</h2>
      <div class="row">
             <div class="container" style="width: 80%;">
					   <form action="../controller/exam_update.php" method="POST">
					   	<input type="hidden" name="exam_id" value="<?php echo $exam['exam_id']; ?>">
						<div class="form-group">
							<label for="name" style="float:left;"><b>Name</b></label>
							<input id="name_id" class="form-control" type="text" name="name" value="<?php echo $exam['name']; ?>"readonly>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Description</b></label>
							<textarea id="desc_id" class="form-control"name="descr" readonly
							rows="10"><?php echo $exam['descr']; ?></textarea>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Level</b></label>
							<input id="lvl_id" class="form-control" type="text"name="lvl" readonly
							value="<?php echo $exam['lvl']; ?>"
							>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Slots</b></label>
							<input id="slot_id" class="form-control" type="text" name="slots"readonly
							value="<?php echo $exam['slots']; ?>"
							>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Rate</b></label>
							<input id="rate_id" class="form-control" type="text" name="rate"readonly
							value="<?php echo $exam['rate']; ?>"
							>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Prerequisite</b></label>
							<textarea class="form-control" id="pre_id" name="pre" placeholder="Prerequisits" readonly rows="10"><?php echo $exam['prerequisit']; ?></textarea>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Duration</b></label>
							<input id="dur_id" class="form-control" type="text" name="duration" readonly
							value="<?php echo $exam['duration']; ?>"
							>
						</div>
						<div class="form-group">
							<label for="name" style="float:left;"><b>Require Documents</b></label>
							 <textarea rows="10" id="req_id" class="form-control" name="req_doc" placeholder="Required Documents" readonly><?php echo $exam['req_doc']; ?></textarea>
						</div>
						
							<br/>
							<button class="btn btn-primary" type="submit" id='btnsave' style="display: none;">Save Changes</button>
							<button class="btn btn-primary" type="button"  id='btncancel' style="display: none;">Cancel</button>
						</form>
			 </div>
      </div>
	  <br/>
    </div>
  </section>
	<script>
		$(document).ready(function(){
			$(document).on('click', 'button[data-role=del]', function(){		
				$('#mediumModal2').modal('toggle');
			});
			$(document).on('click', 'button[data-role=edit]', function(){		
				$('#name_id').removeAttr("readonly");
				$('#desc_id').removeAttr("readonly");
				$('#slot_id').removeAttr("readonly");
				$('#rate_id').removeAttr("readonly");
				$('#lvl_id').removeAttr("readonly");
				$('#pre_id').removeAttr("readonly");
				$('#req_id').removeAttr("readonly");
				$('#from_id').removeAttr("readonly");
				$('#to_id').removeAttr("readonly");
				$('#dur_id').removeAttr("readonly");
				$('#btnsave').css("display", "");
				$('#btncancel').css("display", "");
			});
			
		});
	</script>
	<!-- The Modal -->
  <div class="modal fade" id="mediumModal2">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Are you sure you want to deactivate this exam?</h5>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        	<form action="../controller/exam_delete.php" method="POST">
        		<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
				<button type="submit" class="btn btn-danger">Yes</button>
	          	<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          	</form>
        </div>
      </div>
    </div>
  </div>	
	<?php
		include("footer_main.php");
	?>
</body>

</html>
