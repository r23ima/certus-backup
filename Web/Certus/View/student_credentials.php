<!DOCTYPE html>
<html lang="en">

<head>

  <title>Student Credential</title>
	
	<?php
		include("csslinks.php");
		require_once '../model/prof.php';
		if(isset($_GET['user'])){
			$user = $_GET['user'];
		}

		if(isset($_GET['message'])){
        	$m = $_GET['message'];
        	echo "<script>alert("; 
          		echo $m;
        	echo ")</script>";
      }

      $server_ip = gethostbyname(gethostname());
      $getPic = 'http://'.$server_ip.'/certus/uploads/';
	?>
</head>

<body>
	<?php
		include("header_main.php");
	?>
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Student Credential</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
  <!-- Testimonials -->
  <section class=" bg-light ">
    <div class="container border">
	<br/>
		<h2 class="mb-5">Student Credential</h2>
			<div class="row" style="margin: 5px;">
				<div class="col">
					<div class="card">
						<div class="card-header bg-primary text-white">
						    Enrollee Information
						</div>
						<div class="card-body">
						   		<?php 
						   			require_once '../model/prof.php';
						   			$db = new prof();
									$res = $db->getProf_details($user);
									$iden = $res['prof_id'];
									$pic = $getPic.$res['prof_pic'];
									echo "
											<div class='row'>
												<div class='col-3'>
													<img src='{$pic}' class='img-thumbnail' style='height: 150px; width: 150px;'>
												</div>
												<div class='col-12 col-md-8'>
												  <b>Name: </b><span style='padding-left: 35px;'>{$res['prof_first']}&nbsp; {$res['prof_last']}</span>
												  <br/>
												  <b>Birthdate: </b><span style='padding-left: 8px;'>{$res['prof_birth']}</span><br/>
												  <b>Email: </b><span style='padding-left: 35px;'>{$res['prof_email']}</span>
												  <br/>
												  <b>Address: </b><span style='padding-left: 13px;'>{$res['prof_addr']}</span><br/>
												  <b>Gender: </b><span style='padding-left: 20px;'>{$res['prof_gender']}</span><br/>
												  <b>Status: </b><span style='padding-left: 26px;'>{$res['prof_stat']}</span><br/>
												</div>  
											</div>	
										 ";
								?>
						</div>
					</div>
				</div>	
			</div><!--END OF ENROLEE INFORMATION-->

			<br/>
			<div class="row" style="margin: 5px;">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary text-white">
						    Education Details
						</div>
						<div class="card-body">
						   	<table class="table">
							  <thead class="thead">
							    <tr>
							      <th scope="col">Level</th>
							      <th scope="col">School</th>
							      <th scope="col">Degree</th>
							      <th scope="col">Start</th>
							      <th scope="col">End</th>
							      <th scope="col">Award</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<?php
							  		require_once '../model/educ.php';
						   			$db = new educ();
						   			$res = $db->viewAll($iden);
							  		for($i=0; $i<count($res); $i++){
									    echo "<tr>
									      <th>{$res[$i]['lvl']}</th>
									      <td>{$res[$i]['school']}</td>
									      <td>{$res[$i]['degree']}</td>
									      <td>{$res[$i]['start']}</td>
									      <td>{$res[$i]['end']}</td>
									      <td>{$res[$i]['award']}</td>
									    </tr>";
									}    
								 ?> 	
							  </tbody>
							</table>
						</div>
					</div>	
				</div>	
			</div><!--END OF EDUCATION TABLE-->
			<br/>
			<div class="row" style="margin: 5px;">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary text-white">
						    Portfolios
						</div>
						<div class="card-body">
						   	<table class="table">
							  <thead class="thead">
							    <tr>
							      <th scope="col">Certificate</th>
							      <th scope="col">Provider</th>
							      <th scope="col">Name</th>
							      <th scope="col">Level</th>
							      <th scope="col">Validity</th>
							      <th scope="col">Status</th>
							    </tr>
							  </thead>
							  <tbody>
							  	<?php
							  		require_once '../model/cert.php';
						   			$db = new cert();
						   			$res = $db->viewAllCert($iden);
							  		for($i=0; $i<count($res); $i++){
							  			$pic = $getPic.$res[$i]['cert_pic'];
									    echo "<tr>
									      <th><img src='{$pic}' class='img-thumbnail' style='height: 100px; width: 150px;'></th>
									      <td>{$res[$i]['provider_name']}</td>
									      <td>{$res[$i]['cert_name']}</td>
									      <td>{$res[$i]['cert_lvl']}</td>
									      <td>{$res[$i]['cert_from']} - {$res[$i]['cert_to']}</td>
									      <td>{$res[$i]['cert_stat']}</td>
									    </tr>";
									}    
								 ?> 	
							  </tbody>
							</table>
						</div>
					</div>	
				</div>	
			</div>
	  <br/>
    </div>
  </section>
	<?php
		include("footer_main.php");
	?>
</body>

</html>
