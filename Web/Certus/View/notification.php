<!DOCTYPE html>
<html lang="en">
<head>
  <title>Notifications</title>
	
	<?php
		include("csslinks.php");
     require_once '../model/notification.php';
      $db = new notif();
      $server_ip = gethostbyname(gethostname());
      $getPic = 'http://'.$server_ip.'/certus/uploads/'; 
	?>
</head>
<body>

	<?php
		include("header_main.php");
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Notification</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
      <div class="container">
            <ul class="list-group">
              <?php
                if($count!=0){
                  for ($i=0; $i < $count; $i++) { 
                    if($res[$i]['stat']=="PENDING"){
                      $mes = " has sent a request";
                    }
                    elseif($res[$i]['stat']=="CANCELLED"){
                      $mes = " has sent a request";
                    }

                    $myPic = $getPic.$res[$i]['pic'];
                    echo "
                        <li class='list-group-item'>
                          <a href='request_details.php?id={$res[$i]['id']}'>
                            <span style='float: left;'>
                              <img src='{$myPic}' class='img-thumbnail' style='height: 30px; width: 30px;'>&nbsp;
                              <b>{$res[$i]['full']} {$mes}</b>
                            </span>  
                          </a>  
                            <span style='float: right;'>{$res[$i]['date']}</span>
                        </li>
                        ";
                  }
                }
                else{
                  echo "<li class='list-group-item'>No notification</li>";
                }
              ?>
            </ul>
      </div>
  </section>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
