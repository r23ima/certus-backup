<!DOCTYPE html>
<html lang="en">
<head>
  <title>Payment Details</title>
	
	<?php
		include("csslinks.php");
		if(isset($_GET['message'])){
	        $m = $_GET['message'];
	        echo "<script>alert("; 
	          echo $m;
	        echo ")</script>";
	    }
	    $dis=0;
	    $t=0;
	    $dis2=0;
	    $topay=0;
	?>
</head>
<body>

	<?php
		include("header_main.php");
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Payment</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" placeholder="Search..." aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center">
        <div class="container">
        	<div class="col">
				<table class="text-left">
					<?php
						require_once '../model/request.php';
						$db = new request();
						$res = $db->viewSingleRequest($_GET['id']);
					?>
					<tr>
						<td><b>Name: </b></td>
						<td style="padding-left: 10px;"><?php echo $res['fullname']; ?></td>
					</tr>
					<tr>
						<td><b>Transaction No: </b></td>
						<td style="padding-left: 10px;"><?php echo $res['request_id']; ?></td>
					</tr>
					<tr>
						<td><b>Total Amount: </b></td>
						<td style="padding-left: 10px;"><?php echo $res['total']; ?></td>
					</tr>
					<tr>
						<td><b>Discount: </b></td>
						<td style="padding-left: 10px;"><?php echo $res['discount']; ?> %</td>
					</tr>
					<tr>
						<td><b>Status: </b></td>
						<td style="padding-left: 10px;"><?php echo $res['status']; ?></td>
					</tr>
					<?php
						$dis = $res['discount'];
						$t = $res['total'];
					?>
				</table>
			</div>
			<br/>
			<hr>
			<br/>
                    <div class="row">
                        <div class="col-12">
                            <h3>Payment table</h3>
                            <div><br/>
		                        <button class="fas fa-plus btn btn-primary" style="float:left;" data-role='add'> &nbsp;Add Payment</button>
                            </div>
                            <br/>
                            <div >
                            	<br/>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>   
											<th></th>
											<th>Payment ID</th>
                                            <th>Date Paid</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    	require_once '../model/payment.php';
										$db = new payment();
										$res = $db->viewAllPayment($_GET['id']);
										$sum=0;
                                    		for($i=0; $i<count($res); $i++){
		                                        echo "<tr>
		                                        	<td></td>
		                                            <td>{$res[$i]['pay_id']}</td>
													<td>{$res[$i]['date']}</td>
		                                            <td>{$res[$i]['amount']}</td>   
		                                        </tr>"; 
		                                        $sum += $res[$i]['amount'];
		                                    }
		                                    $topay = number_format($sum, 2);    
                                    	?>
                                    	<tr>
		                                   <th colspan='3' class="text-left">Total Payments</th>
											<th>Php <?php echo $topay; ?></th>
		                                </tr>
		                                <tr>
		                                   <td colspan='3' class="text-left">Discount</td>
											<td><?php echo $dis;?> %</td>
		                                </tr>
		                                <tr>
		                                   <th colspan='3' class="text-left">Total Due</th>
											<th>Php 
												<?php 
									    			if($dis==0){
									    				echo $res['total']; 
									    				$dis2 = 0;
									    			}else{
									    				$d = $dis;
									    				$td = $t;
									    				$ta = 0;
									    				$ta = number_format(($td - ($td * ($d / 100))), 2);
									    				$dis2 = $td * ($d / 100);
									    				echo $ta;
									    			}
									    		?>	
											</th>
		                                </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row text-left">
                    	<div class="col-12">
	                    	<div class="card" style="margin: 10px;">
								<div class="card-header bg-primary text-white">
								    Assessment Slip
								</div>
			                    	<table style="margin: 20px;">
			                    		<tr>
			                    			<td><b>Total Amount Due</b></td>
			                    			<td style="padding-left: 20px;"><?php echo $t;?></td>
			                    		</tr>
			                    		<tr>
			                    			<td><b>Discount <?php echo $dis;?> %</b></td>
			                    			<td style="padding-left: 20px;"><?php echo $dis2;?></td>
			                    		</tr>
			                    		<tr>
			                    			<td><b>Total Payments</b></td>
			                    			<td style="padding-left: 20px;">Php <?php echo $topay;?></td>
			                    		</tr>
			                    		<tr>
			                    			<td><b>Balance</b></td>
			                    			<td style="padding-left: 20px;">Php 
			                    				<?php
			                    					$bal=0;
			                    					$alldis = ($sum + $dis2);
			                    					$bal = ($t - $alldis);
			                    					echo $bal;
			                    				?>
			                    			</td>
			                    		</tr>
			                    	</table>
		                    </div>
		                </div>    	
                    </div>
                </div>
  </section>
  
		<script>
			$(document).ready(function(){
				$(document).on('click', 'button[data-role=edit]', function(){
					var id=$(this).closest('tr').attr('id');
					var sp=$('#'+id).children('td[data-target=sp_id]').text();
			
					$('#sp_id').val(sp);
					$('#mediumModal').modal('toggle');
				});			
			});
		</script>
			<!-- The Modal -->
		  <div class="modal fade" id="mediumModal">
			<div class="modal-dialog">
			  <div class="modal-content">
			  
				<!-- Modal Header -->
				<div class="modal-header">
				  <h4 class="modal-title">Add Payment</h4>
				  <button type="button" class="close" data-dismiss="modal">×</button>
				</div>
				
				<!-- Modal body -->
				<div class="modal-body">
				 <form method="POST" action="../controller/payment_add.php" id="myForm">
					 <input type='hidden' name='id' value='<?php echo $_GET['id']; ?>'>
					 <label>Date Paid: </label>  
	                	<input type="date" class="form-control" name="date" required>
	                <label>Amount</label>
	                	<input type="number" class="form-control" name="amount" placeholder="Amount paid"> &nbsp;
				</div>
				
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">Save</button>
				  <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				</div>
				</form>
			  </div>
			</div>
		  </div>
		  <script>
		    $(document).ready(function(){
		      $(document).on('click', 'button[data-role=add]', function(){    
		        $('#mediumModal').modal('toggle');
		      });
		    });
		  </script>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
