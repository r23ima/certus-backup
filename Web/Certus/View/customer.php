<!DOCTYPE html>
<html lang="en">
<head>
  <title>Customer</title>	
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Transaction</li>
                                </ul>
                        </div>
                            <form action="" method="post">
                                <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
								<button class="au-btn--submit2" type="submit">
									<i class="fas fa-search"></i>
								</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center bg-light ">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Customer table</h3>
                            <div>
                                <div>
                                    <div>
                                        <select name="property">
                                            <option selected="selected">All Properties</option>
                                            <option value="">Active</option>
                                            <option value="">Deactivated</option>
                                        </select>  
                                    </div>
                                </div>    
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>   
											<th>Name</th>
                                            <th>Email</th>
											<th>Address</th>
                                            <th>Number</th>
											<th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Janess Silla</td>
											<td>js69@yahoo.com</td>
                                            <td>N. Escario Street</td>
                                            <td>09333378911</td>    
                                            <td><span class="badge badge-success">Active✔</span></td>
                                        </tr>                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
