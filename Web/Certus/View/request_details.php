<!DOCTYPE html>
<html lang="en">

<head>

  <title>Exam Details</title>
	
	<?php
		include("csslinks.php");
		require_once '../model/request.php';
			$id=0;
		if(isset($_GET['id'])){
			$id = $_GET['id'];
		}

		if(isset($_GET['message'])){
        	$m = $_GET['message'];
        	echo "<script>alert("; 
          		echo $m;
        	echo ")</script>";
      }

	?>
</head>

<body>
	<?php
		include("header_main.php");
	?>
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item active"> <a href="transaction.php">Transaction</a></li>
                                   <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Request Details</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
  <!-- Testimonials -->
  <section class=" bg-light ">
    <div class="container border">
	<br/>
		<div class="row">
			<div class="col">
				<h2 class="mb-5">Request Details</h2>
			</div>
				<?php
				$db = new request();
				$res = $db->viewSingleRequest($id);
				$st= $res['status'];
					if($res['status']=="APPROVE" || $res['status']=="ONGOING"){
						echo "<div class='col'>
							<form action='../controller/request_update_status.php' method='POST'>
								<input type='hidden' value='{$id}' name='id'>
								<button type='submit' class='btn btn-outline-success' style='float: right' name='verdict' value='DONE'>Mark As Done <i class='fas fa-check'></i>
								</button>
								<button type='submit' class='btn btn-outline-danger' style='float: right; margin-right: 10px;' name='verdict' value='DROPPED'>Mark As Dropped <i class='fas fa-times'></i>
								</button>
							</form>
							
						</div>";
					}
				?>
		</div>
			<div class="row" style="margin: 5px;">
				<div class="col">
					<div class="card">
						<div class="card-header bg-primary text-white">
						    Application Details
						</div>
						<div class="card-body">
						   		<?php 
									echo "<b>Request ID: </b> 		<span style='padding-left: 49px;'>{$res['request_id']}</span><br/>
										  <b>Date of Request: </b> 	<span style='padding-left: 10px;'>{$res['date']}</span><br/>
										  <b>Description: </b> 		<span style='padding-left: 42px;'>{$res['descr']}</span><br/>
										  <b>Status: </b> 			<span style='padding-left: 82px;'>{$res['status']}</span><br/>
										  <b>Payment: </b> 			<span style='padding-left: 63px;'>{$res['name']}</span><br/>
										 ";
								?>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card">
						<div class="card-header bg-primary text-white">
						    Enrollee Information
						</div>
						<div class="card-body">
						   		<?php 
									echo "<a href='student_credentials.php?user={$res['prof_user']}' style='float: right; font-size: 12px;'>View Credentials <i class='fas fa-external-link-alt'></i></a>
										  <b>Name: </b> 	<span style='padding-left: 21px;'>{$res['fullname']}</span><br/>
										  <b>Age: </b> 		<span style='padding-left: 35px;'>{$res['age']}</span><br/>
										  <b>Email: </b> 	<span style='padding-left: 21px;'>{$res['prof_email']}</span><br/>
										  <b>Address: </b> 	<span style='padding-left: 1px;'>{$res['prof_addr']}</span><br/>
										 ";
								?>
						</div>
					</div>
				</div>	
			</div>
			<br/>
			<div class="row" style="margin: 5px;">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary text-white">
						    Course Details
						</div>
						<div class="card-body">
						   	<table class="table">
							  <thead class="thead">
							    <tr>
							      <th scope="col">Course ID</th>
							      <th scope="col">Name</th>
							      <th scope="col">Level</th>
							      <th scope="col">Amount</th>
							     
							    </tr>
							  </thead>
							  <tbody>
							  	<?php
							  	$course = $db->viewCourses($res['request_id']);
							  		for($i=0; $i<count($course); $i++){
									    echo "<tr>
									      <th scope='row'>{$course[$i]['id']}</th>
									      <td>{$course[$i]['name']}</td>
									      <td>{$course[$i]['lvl']}</td>
									      <td>{$course[$i]['rate']}</td>
									    ";

									    echo "</tr>";
									}    
								 		
 									if($st=='APPROVE' || $st=='ONGOING'){
										echo " <tr>
									    	<th colspan='3'>Subtotal</th>
									    	<th>Php"; echo number_format($res['total'], 2); 
									    	echo "</th><th></th>
									    </tr>  
									    <tr>
									    	<td colspan='3'>Discount</td>
									    	<td>"; echo $res['discount']; 
									    	echo "%</td><th></th>
									    </tr>
									    <tr>
									    	<th colspan='3'>Total Amount</th>
									    	<th>Php ";
									    		
									    			if($res['discount']==0){
									    				echo $res['total']; 
									    			}else{
									    				$d = $res['discount'];
									    				$t = $res['total'];
									    				$ta = 0;
									    				$ta = number_format(($t - ($t * ($d / 100))), 2);
									    				echo $ta;
									    			}
									    		
									    	echo "</th><th></th>

									    </tr>";
									}

								    ?> 	
							  </tbody>
							</table>
						</div>
					</div>
					<div class='container text-center' style='margin: 10px;'>
						<?php
							if($res['status']=="PENDING"){
								echo "<form action='../controller/request_update_status.php' method='POST'>
									<input type='hidden' value={$res['request_id']} name='id'>
									<input type='submit' class='btn btn-success w-25' value='APPROVE' name='verdict'/>
									<button type='button' data-role='decline' class='btn btn-danger w-25'/>DECLINE</button>
								</form>";
							}	
						?>	
					</div>	
				</div>	
			</div>
	  <br/>
    </div>
  </section>

	<!-- The Modal -->
  <div class="modal fade" id="mediumModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h5 class="modal-title">Reason for declining</h5>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        <form action="../controller/request_update_status.php" method="POST">
        <div class="modal-body">
        	<textarea class="form-control" name="message"></textarea>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
        		<input type="hidden" name="id" value="<?php echo $res['request_id']; ?>">
				<button type='submit' class='btn btn-danger' value='DECLINE' name='verdict'>Submit</button>
	          	<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
          	</form>
        </div>
      </div>
    </div>
  </div>

    <script>
		$(document).ready(function(){
			$(document).on('click', 'button[data-role=decline]', function(){		
				$('#mediumModal').modal('toggle');
			});

			$(document).on('click', 'button[data-role=evaluate]', function(){		
				$('#ModalEv').modal('toggle');
			});
		});
	</script>
	<?php
		include("footer_main.php");
	?>
</body>

</html>
