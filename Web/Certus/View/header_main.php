<?php
  session_start();
  require_once '../model/notification.php';
  $db = new notif();
  $count=0;
    $res = $db->receiveNotificationFromStudents($_SESSION['id']);
    $count = count($res);
?>
 <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-light">
  <a class="navbar-brand" href="#"><img src="img/certus.png" style=" height: 60px; width: 110px;"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="dashboard.php">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="transaction.php">Transaction</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="payment.php">Payment Type</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="feedback.php">Ratings</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="courses.php">Courses</a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="profile.php">Profile</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="notification.php">Notifications
          <span class="badge badge-pill badge-primary">
            <?php echo $count; ?>
          </span>
        </a>
    </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <button class="btn form-control" type="button" style="background-color: darkblue;"><a style="color: gold;" href="../controller/logout.php">Logout</a></button>
    </form>
  </div>
</nav>