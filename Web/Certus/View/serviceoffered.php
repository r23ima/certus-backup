<!DOCTYPE html>
<html lang="en">
<head>
  <title>Services Offered</title>
	
	<?php
		include("csslinks.php");
	?>
</head>
<body>

	<?php
		include("header_main.php");
	?>
	
	<!-- BREADCRUMB-->
    <section class="au-breadcrumb2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">You are here:</span>
                                <ul class="list-unstyled list-inline au-breadcrumb__list">
                                  <li class="list-inline-item active">
                                       <a href="dashboard.php">Home</a>
                                  </li>
                                  <li class="list-inline-item seprate">
									<span>/</span>
                                  </li>
                                  <li class="list-inline-item">Service Provider's services</li>
                                </ul>
                        </div>
                            <form class="form-inline my-2 my-lg-0">
                              <input class="form-control mr-sm-2" type="search" placeholder="Search..." aria-label="Search">
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BREADCRUMB-->
	
  <!-- Testimonials -->
  <section class="testimonials text-center bg-light ">
        <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Services table</h3>    
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>   
											<th>Service ID</th>
											<th>Type</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>0089</td>
											<td>General Cleaning</td>
                                            <td>Cleans and filter</td> 
											<td>₱500.00</td>
                                        </tr>                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
  </section>

	<?php
		include("footer_main.php");
	?>

</body>
</html>
