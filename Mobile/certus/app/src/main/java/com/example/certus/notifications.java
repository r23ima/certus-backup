package com.example.certus;

public class notifications {
    private String url;
    private String name;
    private String status;
    private String request_id;

    public notifications(String url, String name, String status, String request_id) {
        this.url = url;
        this.name = name;
        this.status = status;
        this.request_id = request_id;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getRequest_id() {
        return request_id;
    }

}
