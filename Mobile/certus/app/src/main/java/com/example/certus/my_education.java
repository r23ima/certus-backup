package com.example.certus;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class my_education extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    CardView educ, skill;
    ImageView back, edit_menu, del_menu;
    EditText start, end, school, degree, description;
    TextView title;
    Spinner lvl;
    String currentDateString, currentDate2String, selected_lvl;
    Button save;
    int alt, id_no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_education);
        educ = findViewById(R.id.educ_tab);
        skill = findViewById(R.id.skills_tab);
        start = findViewById(R.id.start_date);
        end = findViewById(R.id.end_date);
        back = findViewById(R.id.backbtn);
        lvl = findViewById(R.id.spinner);
        save = findViewById(R.id.save);
        school = findViewById(R.id.school);
        degree = findViewById(R.id.degree);
        description = findViewById(R.id.description);
        edit_menu = findViewById(R.id.edit_menu);
        del_menu = findViewById(R.id.del_menu);
        title = findViewById(R.id.toolbar_title);
        title.setText("My Education");

        school.setEnabled(false);
        degree.setEnabled(false);
        description.setEnabled(false);
        start.setEnabled(false);
        end.setEnabled(false);
        save.setVisibility(View.GONE);
        alt=0;
        id_no=0;

        start.setOnClickListener(this);
        end.setOnClickListener(this);
        educ.setOnClickListener(this);
        skill.setOnClickListener(this);
        back.setOnClickListener(this);
        edit_menu.setOnClickListener(this);
        del_menu.setOnClickListener(this);
        save.setOnClickListener(this);
        lvl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                school.setEnabled(false);
                degree.setEnabled(false);
                description.setEnabled(false);
                start.setEnabled(false);
                end.setEnabled(false);
                save.setVisibility(View.GONE);
                school.setText("");
                degree.setText("");
                description.setText("");
                start.setText("");
                end.setText("");
                selected_lvl = lvl.getSelectedItem().toString();
                displayEduc();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edit_menu:
                save.setVisibility(View.VISIBLE);
                school.setEnabled(true);
                degree.setEnabled(true);
                description.setEnabled(true);
                start.setEnabled(true);
                end.setEnabled(true);
                start.setFocusable(false);
                end.setFocusable(false);
                break;
            case R.id.del_menu:
                delEduc();
                break;
            case R.id.save:
                if(id_no==0)
                    addEduc();
                else
                    editEduc();
                break;
            case R.id.educ_tab:
                startActivity(new Intent(getApplicationContext(), my_education.class));
                break;
            case R.id.skills_tab:
                startActivity(new Intent(getApplicationContext(), my_skills.class));
                break;
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
            case R.id.start_date:
                alt=1;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
                break;
            case R.id.end_date:
                alt=2;
                DialogFragment datePicker1 = new DatePickerFragment();
                datePicker1.show(getSupportFragmentManager(), "date picker");
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if(alt==1) {
            currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
            start.setText(currentDateString);
        }
        else if(alt==2){
            currentDate2String= DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
            end.setText(currentDate2String);
        }
        alt=0;
    }
    private void displayEduc(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_educ_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            if(jsonArray.length()==0){
                                id_no=0;
                            }
                            else {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    school.setText(obj.getString("school"));
                                    degree.setText(obj.getString("degree"));
                                    description.setText(obj.getString("award"));
                                    start.setText(obj.getString("start"));
                                    end.setText(obj.getString("end"));
                                    id_no = obj.getInt("id");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("lv", selected_lvl);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void addEduc(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_educ_add,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_education.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("prof_id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("school", school.getText().toString());
                params.put("degree", degree.getText().toString());
                params.put("educ_lvl", selected_lvl);
                params.put("educ_start", start.getText().toString());
                params.put("educ_end", end.getText().toString());
                params.put("educ_award", description.getText().toString());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void editEduc(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_educ_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_education.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(id_no));
                params.put("char", "e");
                params.put("school", school.getText().toString());
                params.put("degree", degree.getText().toString());
                params.put("educ_start", start.getText().toString());
                params.put("educ_end", end.getText().toString());
                params.put("educ_award", description.getText().toString());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void delEduc(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_educ_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_education.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(id_no));
                params.put("char", "d");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
