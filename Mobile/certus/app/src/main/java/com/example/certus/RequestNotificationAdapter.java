package com.example.certus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class RequestNotificationAdapter extends RecyclerView.Adapter<RequestNotificationAdapter.requestViewHolder>{
    private Context context;
    private List<notifications> serviceRequestList;
    private RequestNotificationAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClickListener(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){mListener = listener;}

    public RequestNotificationAdapter(Context context, List<notifications> serviceRequestList) {
        this.context = context;
        this.serviceRequestList = serviceRequestList;
    }

    @NonNull
    @Override
    public requestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.notification_item, null);
        requestViewHolder pvh = new requestViewHolder(view, mListener);
        return  pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull requestViewHolder holder, int i) {
        final notifications sr = serviceRequestList.get(i);
        Picasso.get().load(Constants.url_picture+sr.getUrl()).resize(100, 100).into(holder.iv);
        if(sr.getStatus().equals("U"))
            holder.desc.setText(sr.getName()+" has sent a request");
        else if (sr.getStatus().equals("C"))
            holder.desc.setText(sr.getName()+" has cancel a request");
        else if (sr.getStatus().equals("APPROVE"))
            holder.desc.setText(sr.getName()+" has accepted your request");
        else if (sr.getStatus().equals("DECLINE"))
            holder.desc.setText(sr.getName()+" has declined your request");
        else if (sr.getStatus().equals("E"))
            holder.desc.setText(sr.getName()+" has editted a request");
    }

    @Override
    public int getItemCount() {
        return serviceRequestList.size();
    }

    public static class requestViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;
        TextView desc;
        public requestViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            desc = itemView.findViewById(R.id.desc);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onItemClickListener(position);
                        }
                    }
                }
            });
        }
    }

    public String getID(int i){
        final notifications sr = serviceRequestList.get(i);
        return String.valueOf(sr.getRequest_id());
    }
}