package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewNotification extends AppCompatActivity {
    List<exams> examsList;
    SummaryAdapter adapter;
    RecyclerView rv;
    Intent intent;
    TextView title, descr, payment, mes, pname, addr, total;
    ImageView back;
    LinearLayout declinemessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notification);

        back = findViewById(R.id.backbtn);
        title = findViewById(R.id.toolbar_title);
        title.setText("Request Details");
        descr = findViewById(R.id.descr);
        total = findViewById(R.id.total);
        payment = findViewById(R.id.payment);
        mes = findViewById(R.id.mes);
        pname = findViewById(R.id.name);
        addr = findViewById(R.id.addr);
        declinemessage = findViewById(R.id.declinemessage);
        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        intent = getIntent();
        examsList = new ArrayList<>();

        showCourseDetails();
        showRequest();
        changeStat();

        declinemessage.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NotificationTab.class));
                finish();
            }
        });
    }

    private void showCourseDetails(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_course_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String id = obj.getString("id");
                                String name = obj.getString("name");
                                String lvl = obj.getString("lvl");
                                String rate = obj.getString("rate");
                                exams e = new exams(id, name, "", lvl, "", rate, "", "", "", "", "", "", "");
                                examsList.add(e);
                            }
                            adapter = new SummaryAdapter(getApplicationContext(), examsList);
                            rv.setAdapter(adapter);
                            adapter.setOnItemClickListener(new SummaryAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClickListener(int position) {

                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", intent.getStringExtra("req_id"));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showRequest(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_single_transaction,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            String d = "";
                            if(obj.getString("descr")=="")
                                d = "N/A";
                            else
                                d = obj.getString("descr");

                            descr.setText(d);
                            payment.setText(obj.getString("name")+" - Discount: "+obj.getString("discount")+"%");
                            pname.setText(obj.getString("prov_name"));
                            addr.setText(obj.getString("prov_addr"));
                            Double t = Double.valueOf(obj.getString("total"));
                            Double dis = Double.valueOf(obj.getString("discount"));
                            Double ta = (t - (t * (dis / 100)));
                            total.setText(String.valueOf(ta));
                            //Toast.makeText(getApplicationContext(), obj.getString("prov_name"), Toast.LENGTH_SHORT).show();
                            if(obj.getString("Status")=="DECLINED") {
                                declinemessage.setVisibility(View.VISIBLE);
                                mes.setText(obj.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", intent.getStringExtra("req_id"));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void changeStat(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_change_notification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            // Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", intent.getStringExtra("req_id"));
                params.put("stat", "ONGOING");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}