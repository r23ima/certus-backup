package com.example.certus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.feedbackViewHolder>{
    private Context context;
    List<Feedbacks> feedbackList;
    private FeedbackAdapter.OnItemClickListener mListener;

    public FeedbackAdapter(Context context, List<Feedbacks> feedbackList) {
        this.context = context;
        this.feedbackList = feedbackList;
    }

    public interface OnItemClickListener{
        void onSeeFeed(int pos);
    }

    public void setOnItemClickListener(OnItemClickListener listener){mListener=listener;}

    @NonNull
    @Override
    public feedbackViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.feedback_item, null);
        feedbackViewHolder fvh = new feedbackViewHolder(view, mListener);
        return fvh;
    }

    @Override
    public void onBindViewHolder(@NonNull feedbackViewHolder holder, int i) {
        final Feedbacks f = feedbackList.get(i);
        holder.txtcustomer.setText(f.getCustomer());
        holder.txtdescription.setText(f.getReview());
        holder.ratingBar.setRating(Integer.valueOf(f.getRating()));
        holder.txtdate.setText(f.getDate());
        Picasso.get().load(Constants.url_picture+f.getUrl()).resize(80, 80).into(holder.iv);
    }

    @Override
    public int getItemCount() {
        return feedbackList.size();
    }

    public static class feedbackViewHolder extends RecyclerView.ViewHolder{
        ImageView iv;
        TextView txtcustomer, txtdescription, txtdate;
        RatingBar ratingBar;
        public feedbackViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            txtcustomer = itemView.findViewById(R.id.customer);
            txtdescription = itemView.findViewById(R.id.description);
            ratingBar = itemView.findViewById(R.id.ratings);
            txtdate = itemView.findViewById(R.id.date);
            iv = itemView.findViewById(R.id.iv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onSeeFeed(position);
                        }
                    }
                }
            });
        }
    }

}