package com.example.certus;

import java.io.Serializable;

public class cert implements Serializable {
    String id, taker_id, name, level, provider, start, end, image, ach;
    String status;

    public cert(String id, String taker_id, String name, String level, String provider, String start, String end, String image, String ach, String status) {
        this.id = id;
        this.taker_id = taker_id;
        this.name = name;
        this.level = level;
        this.provider = provider;
        this.start = start;
        this.end = end;
        this.image = image;
        this.ach = ach;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getTaker_id() {
        return taker_id;
    }

    public String getName() {
        return name;
    }

    public String getLevel() {
        return level;
    }

    public String getProvider() {
        return provider;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getImage() {
        return image;
    }

    public String getAch() {
        return ach;
    }

    public String getStatus() {
        return status;
    }
}
