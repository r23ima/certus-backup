package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class provider_profile extends AppCompatActivity implements View.OnClickListener {
LinearLayout ratings, serv, loc;
    TextView title, email, company, con, addr, type;
    ImageView back, pic;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_profile);
        ratings = findViewById(R.id.rating_lay);
        serv = findViewById(R.id.course_lay);
        loc = findViewById(R.id.location_lay);
        back = findViewById(R.id.backbtn);
        email = findViewById(R.id.email);
        company = findViewById(R.id.comp);
        con = findViewById(R.id.con);
        addr = findViewById(R.id.addr);
        type = findViewById(R.id.type);
        pic = findViewById(R.id.pic);
        title = findViewById(R.id.toolbar_title);
        title.setText("Institution Profile");
        intent = getIntent();

        ratings.setOnClickListener(this);
        serv.setOnClickListener(this);
        loc.setOnClickListener(this);
        back.setOnClickListener(this);
        displayUser();
    }
    private void displayUser(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_provider_profile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            company.setText(obj.getString("name"));
                            type.setText(obj.getString("name"));
                            addr.setText(obj.getString("addr"));;
                            email.setText(obj.getString("email"));
                            String u = obj.getString("url");
                            String url = Constants.url_picture+u;
                            Picasso.get().load(url).resize(100, 100).into(pic);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", intent.getStringExtra("prov_id"));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        Intent intent01;
        switch (v.getId()){
            case R.id.rating_lay:
                intent01 = new Intent(getApplicationContext(), provider_feedback.class);
                intent01.putExtra("id", intent.getStringExtra("prov_id"));
                startActivity(intent01);
                break;
            case R.id.location_lay:
                intent01 = new Intent(getApplicationContext(), SchoolMap.class);
                intent01.putExtra("id", intent.getStringExtra("prov_id"));
                startActivity(intent01);
                break;
            case R.id.course_lay:
                intent01 = new Intent(getApplicationContext(), Services.class);
                intent01.putExtra("id", intent.getStringExtra("prov_id"));
                startActivity(intent01);
                break;
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_browsing.class));
                finish();
                break;
        }
    }
}
