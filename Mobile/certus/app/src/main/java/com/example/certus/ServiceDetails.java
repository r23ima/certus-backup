package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceDetails extends AppCompatActivity {
Intent intent;
TextView title, require, desc, level, tool_title;
RatingBar ratingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_details);
        title = findViewById(R.id.title);
        require = findViewById(R.id.require);
        title = findViewById(R.id.title);
        desc = findViewById(R.id.descr);
        level =findViewById(R.id.lvl);
        tool_title = findViewById(R.id.toolbar_title);
        tool_title.setText("Service Details");
        intent = getIntent();
        displayDetails();
    }
    private void displayDetails(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_exam_details_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                                JSONObject obj = new JSONObject(response);
                                String id = obj.getString("exam_id");
                                String prov = obj.getString("prov_id");
                                String name = obj.getString("name");
                                String descr = obj.getString("descr");
                                String lvl = obj.getString("lvl");
                                String slots = obj.getString("slots");
                                String rate = obj.getString("rate");
                                String pre = obj.getString("pre");
                                String to = obj.getString("to");
                                String from = obj.getString("from");
                                String duration = obj.getString("duration");
                                String req_doc = obj.getString("req_doc");
                                title.setText(name);
                                desc.setText(descr);
                                require.setText(req_doc);
                                level.setText(lvl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", intent.getStringExtra("id"));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }


}
