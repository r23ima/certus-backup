package com.example.certus;

import java.io.Serializable;

public class exams implements Serializable {
    String exam_id, name, descr, lvl, slots, rate, pre, from, to, duration, req_doc, prov_name, status;


    public exams(String exam_id, String name, String descr, String lvl, String slots, String rate, String pre, String from, String to, String duration, String req_doc, String prov_name, String status) {
        this.exam_id = exam_id;
        this.name = name;
        this.descr = descr;
        this.lvl = lvl;
        this.slots = slots;
        this.rate = rate;
        this.pre = pre;
        this.from = from;
        this.to = to;
        this.duration = duration;
        this.req_doc = req_doc;
        this.prov_name = prov_name;
        this.status = status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExam_id() {
        return exam_id;
    }

    public String getName() {
        return name;
    }

    public String getDescr() {
        return descr;
    }

    public String getLvl() {
        return lvl;
    }

    public String getSlots() {
        return slots;
    }

    public String getRate() {
        return rate;
    }

    public String getPre() {
        return pre;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDuration() {
        return duration;
    }

    public String getReq_doc() {
        return req_doc;
    }

    public String getProv_name() {
        return prov_name;
    }

    public String getStatus() {
        return status;
    }
}
