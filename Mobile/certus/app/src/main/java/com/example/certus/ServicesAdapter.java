package com.example.certus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.requestViewHolder>{
    private Context context;
    private static List<exams> serviceRequestList;
    private ServicesAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClickListener(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){mListener = listener;}

    public ServicesAdapter(Context context, List<exams> serviceRequestList) {
        this.context = context;
        this.serviceRequestList = serviceRequestList;
    }

    @NonNull
    @Override
    public requestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.certificate_request_item, null);
        requestViewHolder pvh = new requestViewHolder(view, mListener);
        return  pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull requestViewHolder holder, int i) {
        final exams e = serviceRequestList.get(i);
        holder.textName.setText(e.getName());
        holder.textViewLevel.setText("Level: "+e.getLvl());
        holder.textViewStat.setText("Remaining Slots: "+e.getSlots());
        holder.textViewPrice.setText("Php "+e.getRate());
        if(e.getStatus().equals("checked")) {
            holder.checkBox.setChecked(true);
        }
        else{
            holder.checkBox.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return serviceRequestList.size();
    }

    public static class requestViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;
        TextView textName, textViewLevel, textViewStat, textViewPrice;
        public requestViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            textViewLevel = itemView.findViewById(R.id.textViewlevel);
            textViewStat = itemView.findViewById(R.id.textViewStat);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            checkBox = itemView.findViewById(R.id.checkBox);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onItemClickListener(position);
                        }
                    }
                }
            });
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(buttonView.isChecked()) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            final exams e = serviceRequestList.get(position);
                            e.setStatus("checked");
                        }
                    }
                    else{
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            final exams e = serviceRequestList.get(position);
                            e.setStatus("unchecked");
                        }
                    }
                }
            });
        }
    }

    public String getID(int i){
        final exams e = serviceRequestList.get(i);
        return e.getExam_id();
    }
    public void filteredList (List<exams> cc){
        serviceRequestList = cc;
        notifyDataSetChanged();
    }
}