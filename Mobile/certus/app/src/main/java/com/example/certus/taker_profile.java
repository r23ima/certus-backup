package com.example.certus;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class taker_profile extends AppCompatActivity implements View.OnClickListener {
ImageView edit, del, back, pic;
    TextView txtname, txtnumber, txtemail, txtusername, txtaddr, txtgen, title;
    String txtlastname, txtfirstname, straddr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taker_profile2);
        edit = findViewById(R.id.edit_menu);
        del = findViewById(R.id.del_menu);
        back = findViewById(R.id.backbtn);
        pic = findViewById(R.id.pic);
        txtname = findViewById(R.id.name);
        txtnumber = findViewById(R.id.con);
        txtemail = findViewById(R.id.email);
        txtaddr = findViewById(R.id.addr);
        txtgen = findViewById(R.id.gen);
        txtusername = findViewById(R.id.username);
        title = findViewById(R.id.toolbar_title);
        title.setText("My Profile");

        edit.setOnClickListener(this);
        del.setOnClickListener(this);
        back.setOnClickListener(this);

        displayUser();
    }
    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to deactivate this account?");
        builder.setNeutralButton("No", null);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deactivateUser();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void editDialog(){
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.taker_edit, null);
        final EditText lname, fname, addr;

        lname = v.findViewById(R.id.lname);
        fname = v.findViewById(R.id.fname);
        addr = v.findViewById(R.id.addr);

        lname.setText(txtlastname);
        fname.setText(txtfirstname);
        addr.setText(straddr);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setNeutralButton("Cancel", null);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateUser(fname.getText().toString(), lname.getText().toString(), addr.getText().toString());
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.edit_menu:
                editDialog();
                break;
            case R.id.del_menu:
                showDialog();
                break;
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
        }
    }

    private void displayUser(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_taker_profile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            txtlastname = obj.getString("lastname");
                            txtfirstname = obj.getString("firstname");

                            straddr = obj.getString("address");
                            txtname.setText(txtfirstname+" "+txtlastname);
                            txtaddr.setText(obj.getString("address"));
                            txtnumber.setText(obj.getString("birth"));
                            txtgen.setText(obj.getString("gender"));
                            txtemail.setText(obj.getString("email"));
                            txtusername.setText(obj.getString("username"));
                            String u = obj.getString("url");
                            String url = Constants.url_picture+u;
                            Picasso.get().load(url).resize(100, 100).into(pic);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", SharedPrefManager.getInstance(getApplicationContext()).getUsername());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void updateUser(final String firstn, final String lastn, final String addr){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_taker_update,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), taker_profile.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("last", lastn);
                params.put("first", firstn);
                params.put("addr", addr);
                params.put("char", "e");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void deactivateUser(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_taker_update,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            SharedPrefManager.getInstance(getApplicationContext()).logout();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("char", "d");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}