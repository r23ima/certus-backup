package com.example.certus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.requestViewHolder>{
    private Context context;
    private static List<exams> serviceRequestList;
    private SummaryAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClickListener(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){mListener = listener;}

    public SummaryAdapter(Context context, List<exams> serviceRequestList) {
        this.context = context;
        this.serviceRequestList = serviceRequestList;
    }

    @NonNull
    @Override
    public requestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.summary_request_item, null);
        requestViewHolder pvh = new requestViewHolder(view, mListener);
        return  pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull requestViewHolder holder, int i) {
        final exams e = serviceRequestList.get(i);
        holder.textName.setText(e.getName());
        holder.textViewPrice.setText("Php "+e.getRate());
    }

    @Override
    public int getItemCount() {
        return serviceRequestList.size();
    }

    public static class requestViewHolder extends RecyclerView.ViewHolder {
        TextView textName, textViewPrice;
        public requestViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onItemClickListener(position);
                        }
                    }
                }
            });
        }
    }

    public String getID(int i){
        final exams e = serviceRequestList.get(i);
        return e.getExam_id();
    }
    public void filteredList (List<exams> cc){
        serviceRequestList = cc;
        notifyDataSetChanged();
    }
}