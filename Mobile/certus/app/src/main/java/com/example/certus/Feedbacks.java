package com.example.certus;

public class Feedbacks {
    private String id;
    private String date;
    private String rating;
    private String review;
    private String customer;
    private String provider;
    private String user;
    private String url;

    public Feedbacks(String id, String date, String rating, String review, String customer, String provider, String user, String url) {
        this.id = id;
        this.date = date;
        this.rating = rating;
        this.review = review;
        this.customer = customer;
        this.provider = provider;
        this.user = user;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getRating() {
        return rating;
    }

    public String getReview() {
        return review;
    }

    public String getCustomer() {
        return customer;
    }

    public String getProvider() {
        return provider;
    }

    public String getUser() {
        return user;
    }

    public String getUrl() {
        return url;
    }
}
