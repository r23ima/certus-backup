package com.example.certus;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class cert_Adapter extends RecyclerView.Adapter<cert_Adapter.certViewHolder>{

    private Context context;
    static List<cert> certList;
    private cert_Adapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onClick(int pos);
        void onEditClick(int pos);
        void onDelClick(int pos);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public cert_Adapter(Context context, List<cert> certList) {
        this.context = context;
        this.certList = certList;
    }

    @NonNull
    @Override
    public certViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.cert_item, null);
        cert_Adapter.certViewHolder avh = new cert_Adapter.certViewHolder(view, mListener);
        return avh;
    }

    @Override
    public void onBindViewHolder(@NonNull certViewHolder holder, int i) {
        final cert c = certList.get(i);
        holder.type.setText(c.getName());
        holder.desc.setText("Provider:"+c.getProvider()+"\n"+"Valid till "+c.getStart()+" - "+c.getEnd());
        holder.level.setText("Level: "+c.getLevel());
        holder.stat.setText("Status: "+c.getStatus());
        Picasso.get().load(Constants.url_picture+c.getImage()).resize(150, 110).into(holder.pic);
        if(c.getStatus().equals("Expired")) {
            holder.cert.setCardBackgroundColor(0xFFFFCCCC);
        }
    }

    @Override
    public int getItemCount() {
        return certList.size();
    }

    public static class certViewHolder extends RecyclerView.ViewHolder{
        TextView type, desc, level, stat;
        ImageView pic, edit, del;
        CardView cert;
        public certViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            type = itemView.findViewById(R.id.textViewType);
            desc = itemView.findViewById(R.id.textViewShortDesc);
            level = itemView.findViewById(R.id.textViewlevel);
            stat = itemView.findViewById(R.id.textViewStat);
            pic = itemView.findViewById(R.id.cert_pic);
            edit = itemView.findViewById(R.id.edit);
            del = itemView.findViewById(R.id.del);
            cert = itemView.findViewById(R.id.cert_card);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onClick(position);
                        }
                    }
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onEditClick(position);
                        }
                    }
                }
            });
            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onDelClick(position);
                        }
                    }
                }
            });
        }
    }

    public String getID(int i){
        final cert c = certList.get(i);
        return c.getId();
    }
    public String getName(int i){
        final cert c = certList.get(i);
        return c.getName();
    }
    public String getLvl(int i){
        final cert c = certList.get(i);
        return c.getLevel();
    }
    public String getArch(int i){
        final cert c = certList.get(i);
        return c.getStart();
    }
    public String getFrom(int i){
        final cert c = certList.get(i);
        return c.getAch();
    }
    public String getTo(int i){
        final cert c = certList.get(i);
        return c.getEnd();
    }
    public String getPic(int i){
        final cert c = certList.get(i);
        return c.getImage();
    }
    public String getProvider(int i){
        final cert c = certList.get(i);
        return c.getProvider();
    }
}
