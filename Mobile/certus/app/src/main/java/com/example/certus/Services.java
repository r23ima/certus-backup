package com.example.certus;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Services extends AppCompatActivity implements View.OnClickListener {
RecyclerView rv;
String iden;
List<exams> examsList;
ServicesAdapter adapter;
TextView title;
ImageView back;
Intent intent;
EditText searchText;
Button bookbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        rv = findViewById(R.id.rv);
        title = findViewById(R.id.toolbar_title);
        title.setText("Services");
        back = findViewById(R.id.backbtn);
        bookbtn = findViewById(R.id.bookbtn);
        searchText = findViewById(R.id.searchText);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        intent=getIntent();
        iden = intent.getStringExtra("id");
        examsList = new ArrayList<>();
        displayServices();

        back.setOnClickListener(this);
        bookbtn.setOnClickListener(this);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchFilter(s.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bookbtn:
                filter();
            break;
            case R.id.backbtn:
                finish();
            break;
        }
    }

    private void displayServices(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_exam_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String id = obj.getString("exam_id");
                                String prov = obj.getString("prov_id");
                                String name = obj.getString("name");
                                String descr = obj.getString("descr");
                                String lvl = obj.getString("lvl");
                                String slots = obj.getString("slots");
                                String rate = obj.getString("rate");
                                String pre = obj.getString("pre");
                                String to = obj.getString("to");
                                String from = obj.getString("from");
                                String duration = obj.getString("duration");
                                String req_doc = obj.getString("req_doc");
                                exams e = new exams(id, name, descr, lvl, slots, rate, pre, "", "", duration, req_doc, prov, "none");
                                examsList.add(e);
                            }
                            adapter = new ServicesAdapter(getApplicationContext(), examsList);
                            rv.setAdapter(adapter);
                            adapter.setOnItemClickListener(new ServicesAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClickListener(int position) {
                                    String id=adapter.getID(position);
                                    Intent intent = new Intent(getApplicationContext(), ServiceDetails.class);
                                    intent.putExtra("id", id);
                                    startActivity(intent);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", iden);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void filter(){
        List<exams> ee = new ArrayList<>();
        for (exams e : examsList) {
            String stat = e.getStatus();
            if (stat.equals("checked")) {
                ee.add(e);
            }
        }
        if(ee.size()==0)
            Toast.makeText(getApplicationContext(),"Please pick one service first", Toast.LENGTH_SHORT).show();
        else {
            Intent intent = new Intent(getApplicationContext(), Summary.class);
            intent.putExtra("id", iden);
            intent.putExtra("selectedList", (Serializable) ee);
            startActivity(intent);
        }
    }

    private void searchFilter(String key){
        List<exams> ee = new ArrayList<>();
        for (exams e : examsList) {
            String name = e.getName().toLowerCase();
            String k = key.toLowerCase();
            if (name.contains(k)) {
                ee.add(e);
            }
        }
        adapter.filteredList(ee);
    }

}
