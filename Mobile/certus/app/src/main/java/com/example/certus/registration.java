package com.example.certus;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class registration extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    EditText lname, fname, addr, birth, email, user, pass;
    Spinner gender;
    Button cust_sign;
    ImageView pic;
    String currentDateString, gendertxt;
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private Uri filePath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        pic = findViewById(R.id.pic);
        cust_sign = findViewById(R.id.register);

        lname = findViewById(R.id.lastname);
        fname = findViewById(R.id.firstname);
        addr = findViewById(R.id.address);
        birth = findViewById(R.id.birthdate);
        email = findViewById(R.id.email);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
        gender = findViewById(R.id.gender);

        birth.setFocusable(false);

        pic.setOnClickListener(this);
        birth.setOnClickListener(this);
        cust_sign.setOnClickListener(this);
        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gendertxt = gender.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.pic:
                showFileChooser();
                break;
            case R.id.birthdate:
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
                break;
            case R.id.register:
                if(bitmap!=null) {
                    userRegistration();
                }
                else Toast.makeText(getApplicationContext(), "Upload a photo first", Toast.LENGTH_SHORT).show();
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                pic.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
        birth.setText(currentDateString);
    }

    private void userRegistration(){
        final String lastname=lname.getText().toString();
        final String firstname=fname.getText().toString();
        final String em=email.getText().toString();
        final String add = addr.getText().toString();
        final String username=user.getText().toString();
        final String b=birth.getText().toString();
        final String password=pass.getText().toString();

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_taker_register,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (!obj.getBoolean("error")) {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(username, "c", obj.getString("id"));
                                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                                finish();
                            } else Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        ){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("last", lastname);
                params.put("first", firstname);
                params.put("email", em);
                params.put("birth", b);
                params.put("gender", gendertxt);
                params.put("user", username);
                params.put("pass", password);
                params.put("stat", "Active");
                params.put("addr", add);
                params.put("lat", "10.297529");
                params.put("lon", "123.896966");
                params.put("pic", imageToString(bitmap));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
