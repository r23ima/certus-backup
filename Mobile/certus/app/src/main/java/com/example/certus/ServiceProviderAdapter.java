package com.example.certus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ServiceProviderAdapter extends RecyclerView.Adapter<ServiceProviderAdapter.providerViewHolder>{
    private Context context;
    private List<ServiceProvider> provider;
    private ServiceProviderAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClickListener(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){mListener = listener;}

    public ServiceProviderAdapter(Context context, List<ServiceProvider> provider) {
        this.context = context;
        this.provider = provider;
    }

    @NonNull
    @Override
    public providerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.provider_item, null);
        providerViewHolder pvh = new providerViewHolder(view, mListener);
        return  pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull providerViewHolder holder, int i) {
        final ServiceProvider serviceProvider = provider.get(i);
        Picasso.get().load(Constants.url_picture+serviceProvider.getUrl()).resize(150, 150).into(holder.iv);
        holder.company.setText(serviceProvider.getCompany());
        holder.address.setText(serviceProvider.getAddress());
        holder.kind.setText(serviceProvider.getKm());
    }

    @Override
    public int getItemCount() {
        return provider.size();
    }

    public static class providerViewHolder extends RecyclerView.ViewHolder {
        TextView company, address, kind;
        RatingBar r;
        ImageView iv;
        CardView p;
        public providerViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            company = itemView.findViewById(R.id.company);
            address = itemView.findViewById(R.id.address);
            r = itemView.findViewById(R.id.rating);
            iv = itemView.findViewById(R.id.notifView);
            p = itemView.findViewById(R.id.ProviderItem);
            kind = itemView.findViewById(R.id.km);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onItemClickListener(position);
                        }
                    }
                }
            });
        }
    }

    public String getID(int i){
        final ServiceProvider sp = provider.get(i);
        return String.valueOf(sp.getProviderID());
    }
    public String getCompany(int i){
        final ServiceProvider sp = provider.get(i);
        return sp.getCompany();
    }
    public void filteredList(List<ServiceProvider> provider2){
        provider = provider2;
        notifyDataSetChanged();
    }
}