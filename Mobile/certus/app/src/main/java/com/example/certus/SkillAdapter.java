package com.example.certus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.providerViewHolder>{
    private Context context;
    private List<skill> skillList;
    private SkillAdapter.OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onEditItemListener(int position);
        void onDelItemListener(int position);
    }

    public  void setOnItemClickListener(OnItemClickListener listener){mListener = listener;}

    public SkillAdapter(Context context, List<skill> skillList) {
        this.context = context;
        this.skillList = skillList;
    }

    @NonNull
    @Override
    public providerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.skill_item, null);
        providerViewHolder pvh = new providerViewHolder(view, mListener);
        return  pvh;
    }

    @Override
    public void onBindViewHolder(@NonNull providerViewHolder holder, int i) {
        final skill s = skillList.get(i);
        holder.skill.setText(s.getDescr());
        holder.special.setText(s.getSp());
    }

    @Override
    public int getItemCount() {
        return skillList.size();
    }

    public static class providerViewHolder extends RecyclerView.ViewHolder {
        TextView skill, special;
        ImageView edit, del;
        public providerViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            skill = itemView.findViewById(R.id.skill);
            special = itemView.findViewById(R.id.special);
            edit = itemView.findViewById(R.id.edit);
            del = itemView.findViewById(R.id.delete);

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onEditItemListener(position);
                        }
                    }
                }
            });
            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onDelItemListener(position);
                        }
                    }
                }
            });

        }
    }

    public String getID(int i){
        final skill s= skillList.get(i);
        return s.getId();
    }
    public String getSP(int i){
        final skill s= skillList.get(i);
        return s.getSp();
    }
    public String getDes(int i){
        final skill s= skillList.get(i);
        return s.getDescr();
    }
}