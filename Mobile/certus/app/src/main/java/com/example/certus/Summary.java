package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Summary extends AppCompatActivity{
    List<exams> examsList;
    SummaryAdapter adapter;
    RecyclerView rv;
    Intent intent;
    Double total;
    TextView totaltxt;
    RadioGroup rgp;
    Button bookbtn;
    String pay_id, iden;
    EditText descr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        totaltxt = findViewById(R.id.total);
        bookbtn = findViewById(R.id.bookbtn);
        rgp = findViewById(R.id.radiogroup);
        descr = findViewById(R.id.description);
        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        intent = getIntent();

        examsList = new ArrayList<>();
        examsList = (List<exams>) intent.getSerializableExtra("selectedList");
        adapter = new  SummaryAdapter(getApplicationContext(), examsList);
        rv.setAdapter(adapter);

        iden = intent.getStringExtra("id");
        totalAmount();
        displayPayment();

        rgp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton cb = findViewById(checkedId);
                pay_id = String.valueOf(cb.getId());
            }
        });

        bookbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTransaction();
            }
        });
    }


    private void displayPayment(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_paytype_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String dis="0";
                                    if(obj.getString("dis")=="")
                                        dis="None";
                                    else
                                        dis=obj.getString("dis")+" %";
                                RadioButton rbn = new RadioButton(getApplicationContext());
                                rbn.setId(Integer.valueOf(obj.getString("paytype_id")));
                                rbn.setText(obj.getString("type")+" - Discount: "+dis);
                                rgp.addView(rbn);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", iden);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void addTransaction(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_request_add,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                                if(!obj.getBoolean("error")) {
                                    String id = obj.getString("id");
                                    for (exams e : examsList) {
                                        addTaker(id, e.getExam_id());
                                    }
                                   // Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), taker_dash.class));
                                    finish();
                                }
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("prof_id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("prov_id", iden);
                params.put("paytype_id", pay_id);
                params.put("total", String.valueOf(total));
                params.put("descr", descr.getText().toString());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void addTaker(final String req_id, final String ex_id){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_taker_add,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("request_id", req_id);
                params.put("exam_id", ex_id);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void totalAmount(){
        total=0.00;
        for (exams e : examsList) {
            Double rate = Double.valueOf(e.getRate());
            total +=rate;
        }
        totaltxt.setText("Php "+String.valueOf(total));
    }

}
