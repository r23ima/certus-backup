package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class my_cert extends AppCompatActivity implements View.OnClickListener {
Button add_cert;
ImageView back;
CardView ongoing, port;
    TextView title;
    RecyclerView recyclerView;
    List<cert> certList;
    cert_Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cert);
        add_cert = findViewById(R.id.add_cert);
        back = findViewById(R.id.backbtn);
        ongoing = findViewById(R.id.ongoing_tab);
        port = findViewById(R.id.port_tab);
        title = findViewById(R.id.toolbar_title);
        title.setText("My Portfolios");
        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        certList = new ArrayList<>();

        displayCert();
        ongoing.setOnClickListener(this);
        port.setOnClickListener(this);
        add_cert.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ongoing_tab:
                startActivity(new Intent(getApplicationContext(), ongoing_cert.class));
                break;
            case R.id.add_cert:
                startActivity(new Intent(getApplicationContext(), add_cert.class));
                break;
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
        }
    }
    private void delCert(final String id){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_cert_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_cert.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", id);
                params.put("char", "d");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void displayCert(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_cert_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String cert_id = obj.getString("cert_id");
                                String prof_id = obj.getString("prof_id");
                                String provider_name = obj.getString("provider_name");
                                String cert_name = obj.getString("cert_name");
                                String cert_lvl = obj.getString("cert_lvl");
                                String cert_add = obj.getString("cert_add");
                                String cert_ach = obj.getString("cert_ach");
                                String cert_from = obj.getString("cert_from");
                                String cert_to = obj.getString("cert_to");
                                String cert_pic = obj.getString("cert_pic");
                                String cert_stat = obj.getString("cert_stat");
                                cert c = new cert(cert_id, prof_id, cert_name, cert_lvl, provider_name, cert_from, cert_to, cert_pic, cert_ach, cert_stat);
                                certList.add(c);
                            }
                            adapter = new cert_Adapter(getApplicationContext(), certList);
                            recyclerView.setAdapter(adapter);
                            adapter.setOnItemClickListener(new cert_Adapter.OnItemClickListener() {
                                @Override
                                public void onClick(int pos) {

                                }

                                @Override
                                public void onEditClick(int pos) {
                                    Intent intent = new Intent(getApplicationContext(), add_cert.class);
                                    intent.putExtra("id", adapter.getID(pos));
                                    intent.putExtra("name", adapter.getName(pos));
                                    intent.putExtra("lvl",adapter.getLvl(pos));
                                    intent.putExtra("ach",adapter.getArch(pos));
                                    intent.putExtra("from",adapter.getFrom(pos));
                                    intent.putExtra("to",adapter.getTo(pos));
                                    intent.putExtra("pic",adapter.getPic(pos));
                                    intent.putExtra("prov",adapter.getProvider(pos));
                                    startActivity(intent);
                                }

                                @Override
                                public void onDelClick(int pos) {
                                    delCert(adapter.getID(pos));
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("char", "all");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
