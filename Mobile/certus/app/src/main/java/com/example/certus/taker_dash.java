package com.example.certus;

import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class taker_dash extends AppCompatActivity implements View.OnClickListener {
    LinearLayout profile, logout, browse, education, port, notif;
    TextView refresh, us, textcount;
    String t, m;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taker_dash);
        profile = findViewById(R.id.profile);
        logout = findViewById(R.id.logout);
        browse = findViewById(R.id.browse);
        refresh = findViewById(R.id.refresh);
        notif = findViewById(R.id.notif);
        education = findViewById(R.id.education);
        textcount = findViewById(R.id.textCount);
        port = findViewById(R.id.port);
        us = findViewById(R.id.us);

        us.setText(SharedPrefManager.getInstance(getApplicationContext()).getUsername());

        profile.setOnClickListener(this);
        browse.setOnClickListener(this);
        logout.setOnClickListener(this);
        port.setOnClickListener(this);
        notif.setOnClickListener(this);
        refresh.setOnClickListener(this);
        education.setOnClickListener(this);

        displayExpCert();
        showNotif();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.notif:
                startActivity(new Intent(getApplicationContext(), NotificationTab.class));
                break;
            case R.id.profile:
                startActivity(new Intent(getApplicationContext(), taker_profile.class));
                break;
            case R.id.browse:
                startActivity(new Intent(getApplicationContext(), taker_browsing.class));
                break;
            case R.id.education:
                startActivity(new Intent(getApplicationContext(), my_education.class));
                break;
            case R.id.port:
                startActivity(new Intent(getApplicationContext(), my_cert.class));
                finish();
                break;
            case R.id.refresh:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
            case R.id.logout:
                SharedPrefManager.getInstance(getApplicationContext()).logout();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
        }
    }

    private void showNotif(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_notification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String stat = obj.getString("stat");
                                String person = obj.getString("full");
                                if(stat.equals("APPROVE")){
                                    t = "Request Approved";
                                    m = person+" has successfully approved your request";
                                }
                                else if(stat.equals("DECLINE")){
                                    t = "Request Decline";
                                    m = person+" has successfully declined your request";
                                }
                                textcount.setText(String.valueOf(jsonArray.length()));
                                NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
                                NotificationCompat.Builder nb = notificationHelper.getChannelNotification(t, m);
                                notificationHelper.getManager().notify(1, nb.build());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void displayExpCert(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_cert_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i=0; i<jsonArray.length(); i++){
                                JSONObject obj = jsonArray.getJSONObject(i);
                                NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
                                NotificationCompat.Builder nb = notificationHelper.getChannelNotification("Certificate Alert", "You have " + String.valueOf(jsonArray.length()) + " expired Certificate(s)");
                                notificationHelper.getManager().notify(1, nb.build());
                                updateStatus(obj.getString("id"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("char", "exp");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void updateStatus(final String id){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_cert_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", id);
                params.put("char", "ex");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
