package com.example.certus;

public class ServiceProvider {
    private  int providerID;
    private String company;
    private String email;
    private String address;
    private String url;
    private String lat;
    private String lon;
    private String km;

    public ServiceProvider(int providerID, String company, String email, String address, String url, String lat, String lon, String km) {
        this.providerID = providerID;
        this.company = company;
        this.email = email;
        this.address = address;
        this.url = url;
        this.lat = lat;
        this.lon = lon;
        this.km = km;
    }

    public int getProviderID() {
        return providerID;
    }

    public String getCompany() {
        return company;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getUrl() {
        return url;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getKm() {
        return km;
    }
}
