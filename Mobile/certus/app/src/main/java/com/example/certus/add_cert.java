package com.example.certus;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class add_cert extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    ImageView pic;
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private Uri filePath;
    EditText start, end, cn, provider, lvl, ach;
    String currentDateString, currentDate2String, currentDate3String ;
    Button save;
    Intent intent;
    int alt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cert);
        start = findViewById(R.id.start_date);
        end = findViewById(R.id.end_date);
        ach = findViewById(R.id.date_achieve);
        pic = findViewById(R.id.pic);
        provider = findViewById(R.id.provider);
        cn = findViewById(R.id.cn);
        lvl = findViewById(R.id.lvl);
        save = findViewById(R.id.save);
        intent = getIntent();

        if(intent.hasExtra("id")){
            start.setText(intent.getStringExtra("from"));
            end.setText(intent.getStringExtra("to"));
            ach.setText(intent.getStringExtra("ach"));
            Picasso.get().load(Constants.url_picture+intent.getStringExtra("pic")).resize(150, 110).into(pic);
            provider.setText(intent.getStringExtra("prov"));
            cn.setText(intent.getStringExtra("name"));
            lvl.setText(intent.getStringExtra("lvl"));
        }

        start.setFocusable(false);
        end.setFocusable(false);
        ach.setFocusable(false);

        pic.setOnClickListener(this);
        start.setOnClickListener(this);
        end.setOnClickListener(this);
        ach.setOnClickListener(this);
        save.setOnClickListener(this);
        alt=0;
    }
    private void addCert(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_cert_add,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                                if(!obj.getBoolean("error")){
                                    startActivity(new Intent(getApplicationContext(), my_cert.class));
                                    finish();
                                }
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("prof_id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("cert_name", cn.getText().toString());
                params.put("provider_name", provider.getText().toString());
                params.put("cert_lvl", lvl.getText().toString());
                params.put("provider_name", provider.getText().toString());
                params.put("cert_ach", ach.getText().toString());
                params.put("cert_from", start.getText().toString());
                params.put("cert_to", end.getText().toString());
                params.put("pic", imageToString(bitmap));
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void editCert(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_cert_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_cert.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("cert_name", cn.getText().toString());
                params.put("cert_lvl", lvl.getText().toString());
                params.put("provider_name", provider.getText().toString());
                params.put("cert_ach", ach.getText().toString());
                params.put("cert_from", start.getText().toString());
                params.put("cert_to", end.getText().toString());
                params.put("cert_id",intent.getStringExtra("id"));
                params.put("char", "e");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.start_date:
                alt=1;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
                break;
            case R.id.end_date:
                alt=2;
                DialogFragment datePicker1 = new DatePickerFragment();
                datePicker1.show(getSupportFragmentManager(), "date picker");
                break;
            case R.id.date_achieve:
                alt=3;
                DialogFragment datePicker2 = new DatePickerFragment();
                datePicker2.show(getSupportFragmentManager(), "date picker");
                break;
            case R.id.save:
                if(intent.hasExtra("id"))
                    editCert();
                 else
                    addCert();
                break;
            case R.id.pic:
                showFileChooser();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                pic.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        if(alt==1) {
            currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
            start.setText(currentDateString);
        }
        else if(alt==2){
            currentDate2String= DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
            end.setText(currentDate2String);
        }
        else if(alt==3){
            currentDate3String= DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
            ach.setText(currentDate3String);
        }
        alt=0;
    }

}
