package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class taker_browsing extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclerView;
    List<ServiceProvider> providerList;
    ServiceProviderAdapter adapter;
    Double my_lat, my_lon;
    TextView title;
    ImageView back;
    EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taker_browsing);
        back = findViewById(R.id.backbtn);
        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        title = findViewById(R.id.toolbar_title);
        title.setText("Browse Institutions");
        search = findViewById(R.id.searchText);
        providerList = new ArrayList<>();


         displayMyLoc();

        back.setOnClickListener(this);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }
    private void filter(String key){
        List<ServiceProvider> filteredProvider = new ArrayList<>();
        String k = key.toLowerCase();
        for(ServiceProvider p : providerList){
            String c = p.getCompany().toLowerCase();
            if(c.contains(k)){
                filteredProvider.add(p);
            }
        }
        adapter.filteredList(filteredProvider);
    }
    private void displayProviders(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_provider_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                int id = obj.getInt("id");
                                String company = obj.getString("name");
                                String addr = obj.getString("addr");
                                String email = "Type: " + obj.getString("email");
                                String lat = obj.getString("lat");
                                String lon = obj.getString("lon");
                                String url = obj.getString("pic");
                                ServiceProvider s = new ServiceProvider(id,
                                        company, email, addr, url, lat, lon, getDistance(my_lat, my_lon, Double.valueOf(lat), Double.valueOf(lon)));
                                providerList.add(s);

                            }
                            adapter = new ServiceProviderAdapter(getApplicationContext(), providerList);
                            recyclerView.setAdapter(adapter);
                            adapter.setOnItemClickListener(new ServiceProviderAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClickListener(int position) {
                                    String id=adapter.getID(position);
                                    Intent intent = new Intent(getApplicationContext(), provider_profile.class);
                                    intent.putExtra("prov_id", id);
                                    startActivity(intent);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", SharedPrefManager.getInstance(getApplicationContext()).getUsername());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
        }
    }

    public static String getDistance(double lat_a, double lng_a, double lat_b, double lng_b) {
        double earthRadius = 3958.75;
        double latDiff = Math.toRadians(lat_b - lat_a);
        double lngDiff = Math.toRadians(lng_b - lng_a);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2)
                + Math.cos(Math.toRadians(lat_a))
                * Math.cos(Math.toRadians(lat_b)) * Math.sin(lngDiff / 2)
                * Math.sin(lngDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = earthRadius * c;

        double kmConvertion = 1.6093;

        return String.format("%.2f", new Float(distance * kmConvertion).floatValue()) + " km";
    }

    private void displayMyLoc(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_taker_profile,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            my_lat = Double.valueOf(obj.getString("lat"));
                            my_lon = Double.valueOf(obj.getString("lon"));
                            displayProviders();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", SharedPrefManager.getInstance(getApplicationContext()).getUsername());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
