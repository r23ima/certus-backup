package com.example.certus;

public class skill {
    String id, descr, sp;

    public skill(String id, String descr, String sp) {
        this.id = id;
        this.descr = descr;
        this.sp = sp;
    }

    public String getId() {
        return id;
    }

    public String getDescr() {
        return descr;
    }

    public String getSp() {
        return sp;
    }
}
