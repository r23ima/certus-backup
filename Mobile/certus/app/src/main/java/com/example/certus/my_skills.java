package com.example.certus;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class my_skills extends AppCompatActivity implements View.OnClickListener {
    CardView educ, skill;
    ImageView back;
    RecyclerView recyclerView;
    List<skill> skillList;
    SkillAdapter adapter;
    TextView title;
    Button save;
    String cattxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_skills);
        educ = findViewById(R.id.educ_tab);
        skill = findViewById(R.id.skills_tab);
        back = findViewById(R.id.backbtn);
        save = findViewById(R.id.save);
        title = findViewById(R.id.toolbar_title);
        title.setText("My Skills");
        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        skillList = new ArrayList<>();
        displaySkill();
        educ.setOnClickListener(this);
        skill.setOnClickListener(this);
        back.setOnClickListener(this);
        save.setOnClickListener(this);
    }
    private void addDialog(){
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_add_skill, null);
        final EditText sp;
        final Spinner categ;
        sp = v.findViewById(R.id.special);
        categ = v.findViewById(R.id.skill_spinner);
        categ.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cattxt = categ.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setNeutralButton("Cancel", null);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addSkill(cattxt, sp.getText().toString());
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void editDialog(String cat, String sptxt, final String id){
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.activity_add_skill, null);
        TextView lbl;
        final EditText sp;
        final Spinner categ;
        sp = v.findViewById(R.id.special);
        lbl = v.findViewById(R.id.lbl);
        lbl.setText("Edit Skill");
        lbl.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_edit, 0, 0, 0);
        categ = v.findViewById(R.id.skill_spinner);
        categ.setSelection(((ArrayAdapter<String>)categ.getAdapter()).getPosition(cat));
        categ.setEnabled(false);
        sp.setText(sptxt);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        builder.setNeutralButton("Cancel", null);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editSkill(id, sp.getText().toString());
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void displaySkill(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_skill_view,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String id = obj.getString("id");
                                String prof_id = obj.getString("prof_id");
                                String descr = obj.getString("descr");
                                String sp = obj.getString("sp");
                                skill s = new skill(id, descr, sp);
                                skillList.add(s);
                            }
                            adapter = new SkillAdapter(getApplicationContext(), skillList);
                            recyclerView.setAdapter(adapter);
                            adapter.setOnItemClickListener(new SkillAdapter.OnItemClickListener() {
                                @Override
                                public void onEditItemListener(int position) {
                                    String id = adapter.getID(position);
                                    String sp = adapter.getSP(position);
                                    String des = adapter.getDes(position);
                                    editDialog(des, sp, id);
                                }

                                @Override
                                public void onDelItemListener(int position) {
                                    delSkill(adapter.getID(position));
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void editSkill(final String id, final String sp){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_skill_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_skills.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", id);
                params.put("special", sp);
                params.put("char", "e");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void addSkill(final String cat, final String sp){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_skill_add,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_skills.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("prof_id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("descr", cat);
                params.put("special", sp);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void delSkill(final String id){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_skill_updates,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                startActivity(new Intent(getApplicationContext(), my_skills.class));
                                finish();
                            }
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("char", "d");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.educ_tab:
                startActivity(new Intent(getApplicationContext(), my_education.class));
                break;
            case R.id.skills_tab:
                startActivity(new Intent(getApplicationContext(), my_skills.class));
                break;
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
            case R.id.save:
                addDialog();
                break;
        }
    }
}
