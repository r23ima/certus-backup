package com.example.certus;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class provider_feedback extends AppCompatActivity implements View.OnClickListener {
    LinearLayout rl;
    RatingBar rate;
    EditText review;
    ImageButton save;
    TextView title;
    ImageView back;

    RecyclerView recyclerView;
    List<Feedbacks> feedbackList;
    FeedbackAdapter adapter;
    Intent intent;

    EditText input_id;
    RatingBar r1;
    EditText r2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_feedback);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        title = findViewById(R.id.toolbar_title);
        title.setText("Feedbacks and Ratings");

        rl = findViewById(R.id.createBox);

        if (SharedPrefManager.getInstance(getApplicationContext()).getType().equals("sp")) {
            rl.setVisibility(View.GONE);
        } else {
            rate = findViewById(R.id.ratingBar);
            review = findViewById(R.id.writefeed);
            save = findViewById(R.id.savefeed);
            save.setOnClickListener(this);
        }


        feedbackList = new ArrayList<>();
        intent = getIntent();
        display();
    }

    private void createFeedback() {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_feedback_create,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            rate.setRating(0);
                            review.setText("");
                            Intent in = new Intent(getApplicationContext(), provider_feedback.class);
                            in.putExtra("id", intent.getStringExtra("id"));
                            startActivity(in);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customer", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("provider", intent.getStringExtra("id"));
                params.put("rating", String.valueOf(rate.getRating()));
                params.put("review", review.getText().toString());
                params.put("username", SharedPrefManager.getInstance(getApplicationContext()).getUsername());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void display() {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_feedback_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String id = obj.getString("id");
                                String date = obj.getString("date");
                                String rating = obj.getString("rating");
                                String review = obj.getString("review");
                                String customer = obj.getString("customer");
                                String provider = obj.getString("provider");
                                String us = obj.getString("username");
                                String u = obj.getString("url");
                                Feedbacks f = new Feedbacks(id, date, rating, review, customer, provider, us, u);
                                feedbackList.add(f);
                            }
                            adapter = new FeedbackAdapter(getApplicationContext(), feedbackList);
                            recyclerView.setAdapter(adapter);
                            if (SharedPrefManager.getInstance(getApplicationContext()).getType().equals("c")) {
                                adapter.setOnItemClickListener(new FeedbackAdapter.OnItemClickListener() {
                                    @Override
                                    public void onSeeFeed(int pos) {
                                        String s = feedbackList.get(pos).getUser();
                                        String sh = SharedPrefManager.getInstance(getApplicationContext()).getUsername();
                                        if (s.equals(sh)) {
                                            showDialog();
                                            r1.setRating(Integer.valueOf(feedbackList.get(pos).getRating()));
                                            r2.setText(feedbackList.get(pos).getReview());
                                            input_id.setText(feedbackList.get(pos).getId());
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (SharedPrefManager.getInstance(getApplicationContext()).getType().equals("sp")) {
                    params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                } else {
                    params.put("id", intent.getStringExtra("id"));
                }
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showDialog() {
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.edit_feedback, null);
        r1 = v.findViewById(R.id.ratingBar);
        r2 = v.findViewById(R.id.writefeed);
        input_id = v.findViewById(R.id.feed_id);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Choose an action?");
        builder.setView(v);
        builder.setNeutralButton("Cancel", null);
        builder.setPositiveButton("Submit Changes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int getRat = (int) r1.getRating();
                editFeedback(String.valueOf(getRat), r2.getText().toString(), input_id.getText().toString());
                finish();
                Intent i = new Intent(getApplicationContext(), provider_feedback.class);
                i.putExtra("id", intent.getStringExtra("id"));
                startActivity(i);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void editFeedback(final String rat, final String rev, final String id) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_feedback_edit,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            rate.setRating(0);
                            review.setText("");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customer", SharedPrefManager.getInstance(getApplicationContext()).getId());
                params.put("rating", rat);
                params.put("review", rev);
                params.put("id", id);
                params.put("android", "android");
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.savefeed:
                createFeedback();
                break;
        }
    }
}
