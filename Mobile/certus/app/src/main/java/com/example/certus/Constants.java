package com.example.certus;

public class Constants {
    private static String root_url ="http://192.168.1.9/certus/controller/";

    public static String url_picture = "http:\\/\\/192.168.1.9\\/certus\\/..\\/uploads\\/";

    public static String url_login = root_url+"user_login.php";

    public static String url_taker_profile = root_url+"prof_profile.php";
    public static String url_taker_update = root_url+"prof_updates.php";
    public static String url_taker_register = root_url+"prof_register.php";

    public static String url_provider_list = root_url+"prov_list.php";
    public static String url_provider_profile = root_url+"prov_profile_mobile.php";

    public static String url_cert_add = root_url+"cert_add.php";
    public static String url_cert_view = root_url+"cert_view.php";
    public static String url_cert_updates = root_url+"cert_updates.php";

    public static String url_skill_view = root_url+"skill_view.php";
    public static String url_skill_add = root_url+"skill_add.php";
    public static String url_skill_updates = root_url+"skill_updates.php";

    public static String url_educ_add = root_url+"educ_add.php";
    public static String url_educ_view = root_url+"educ_view.php";
    public static String url_educ_updates = root_url+"educ_updates.php";

    public static String url_exam_view= root_url+"exam_view_mobile.php";
    public static String url_exam_details_view= root_url+"exam_view_details_mobile.php";

    public static String url_paytype_view= root_url+"paytype_view_mobile.php";

    public static String url_request_add= root_url+"request_add.php";
    public static String url_taker_add= root_url+"taker_add.php";

    public static String url_notification= root_url+"notification_mobile_view.php";
    public static String url_change_notification= root_url+"request_update_status_mobile.php";

    public static String url_single_transaction= root_url+"transaction_single_view.php";

    public static String url_course_view= root_url+"course_view_mobile.php";

    public static String url_feedback_list = root_url+"feedback_list.php";
    public static String url_feedback_create = root_url+"feedback_create.php";
    public static String url_feedback_edit = root_url+"feedback_edit.php";

}
