package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationTab extends AppCompatActivity {
    RecyclerView rv;
    List<notifications> list;
    RequestNotificationAdapter adapter;
    ImageView back;
    TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_tab);
        back = findViewById(R.id.backbtn);
        title = findViewById(R.id.toolbar_title);
        title.setText("Notification");
        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));

        list=new ArrayList<>();

        showNotification();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
            }
        });
    }

    private void showNotification(){
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.url_notification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    String serv_id = obj.getString("id");
                                    String name = obj.getString("full");
                                    String url = obj.getString("pic");
                                    String stat = obj.getString("stat");
                                    notifications s = new notifications(url, name, stat, serv_id);
                                    list.add(s);
                                }
                                adapter = new RequestNotificationAdapter(getApplicationContext(), list);
                                rv.setAdapter(adapter);
                                adapter.setOnItemClickListener(new RequestNotificationAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClickListener(int position) {
                                        String id = adapter.getID(position);
                                        Intent intent = new Intent(getApplicationContext(), ViewNotification.class);
                                        intent.putExtra("req_id", id);
                                        startActivity(intent);
                                    }
                                });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ){
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", SharedPrefManager.getInstance(getApplicationContext()).getId());
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }
}
