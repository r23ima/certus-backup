package com.example.certus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ongoing_cert extends AppCompatActivity implements View.OnClickListener {
    CardView ongoing, port;
    ImageView back;
    TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ongoing_cert);
        ongoing = findViewById(R.id.ongoing_tab);
        back = findViewById(R.id.backbtn);
        port = findViewById(R.id.port_tab);
        title = findViewById(R.id.toolbar_title);
        title.setText("Ongoing Certificates");

        ongoing.setOnClickListener(this);
        port.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ongoing_tab:
                startActivity(new Intent(getApplicationContext(), ongoing_cert.class));
                break;
            case R.id.port_tab:
                startActivity(new Intent(getApplicationContext(), my_cert.class));
                break;
            case R.id.backbtn:
                startActivity(new Intent(getApplicationContext(), taker_dash.class));
                finish();
                break;
        }
    }
}
